function showSelectionDialog(selectionType){
    let type = '';
    if (selectionType=='task'){
        type = 'Task';
    }
    else{
        type = 'System';
    }
    $('#select'+type+"DialogBackground")[0].style.display='block';
}
function confirm(selectionType){
    let value = '';
    let showValue = '';
    let checkboxes = $("."+selectionType+"s-item-checkbox");
    let type = '';
    if (selectionType=='task'){
        type = 'Task';
    }
    else{
        type = 'System';
    }
    for(let i=0;i<checkboxes.length;i++){
        if (checkboxes[i].checked){
            let id = checkboxes[i].id.replace(/[a-z]*-item-/,'');
            let name = checkboxes[i].value;
            value+=id+",";
            showValue+=name+',';
        }
    }
    $('#associated'+type)[0].value = value;
    $('#associated'+type+'ShowValue')[0].value = showValue;
    resetCheckbox(selectionType);
    $('#select'+type+'DialogBackground')[0].style.display = 'none';
}
function cancel(selectionType){
    let type = '';
    if (selectionType=='task'){
        type = 'Task';
    }
    else{
        type = 'System';
    }
    $('#select'+type+"DialogBackground")[0].style.display = 'none';
    resetCheckbox(selectionType)
}

function resetCheckbox(selectionType){
    let type = '';
    if (selectionType=='task'){
        type = 'Task';
    }
    else{
        type = 'System';
    }
    let checkboxes = $('.'+selectionType+'s-item-checkbox');
    for(let i=0;i<checkboxes.length;i++){
        if (checkboxes[i].checked){
            checkboxes[i].click();
        }
    }
}
function search(searchElement,objects){
    let input = searchElement.value;
    if (input=='' || input == null){
        showAll(objects);
        return;
    }
    for(let i=0;i<objects.length;i++){
        if (!objects[i].getElementsByTagName('label')[0].innerText.includes(input)){
            objects[i].style.display = 'none';
        }
    }
}
function inputChange(input,objects){
    if (input.value == null || input.value==''){
        showAll(objects);
    }
}
function showAll(objects){
    for(let i=0;i<objects.length;i++){
        objects[i].style.display = 'block';
    }
}

function selectItem(box,ul){

    if (box.checked){
        addItem(box.id,$(box).siblings("label")[0].innerText,ul);
    }
    else {
        removeItem($("#select-"+box.id)[0],ul)
    }
}
function addItem(id,name,ul){
    let curLis = ul.getElementsByTagName("li");
    let newLi = document.createElement('li');
    newLi.setAttribute('class','list-group-item');
    newLi.setAttribute('id','select-'+id);
    newLi.innerText = name;
    newLi.onclick = function (){
        $('#'+id)[0].click();
    };
    ul.appendChild(newLi);
}
function removeItem(li,ul){
    let curLi = ul.getElementsByTagName("li");
    ul.removeChild(li);
}