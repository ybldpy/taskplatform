<%--
  Created by IntelliJ IDEA.
  User: acer
  Date: 2023/2/23
  Time: 14:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<c:import url="/resources/inc/head.jsp"/>
<c:import url="/resources/inc/footer.jsp"/>
<html>
<head>
    <title>任务关联</title>
</head>

<body>
<div class="main">
    <table id="tasks-table"></table>
</div>
<script>


    $(function (){
          $('#tasks-table').bootstrapTable({
              url:"${pageContext.servletContext.contextPath}/manage/task/taskAssociationList?taskId=${taskId}",
              queryParamsType:"limit",
              search:true,
              pagination:true,
              height:getHeight(),
              striped:true,
              showRefresh:true,
              sidePagination:'server',
              detailView:true,
              detailViewByClick:true,
              idField:"taskId",
              detailFormatter:function (index,row,element){
                  return "<textarea class='form-control' style='resize: none' readonly>"+row["description"]+"</textarea>"
              },
              columns:[
                  {
                      title:"编号",
                      field:"taskId",
                      align: "center"
                  },
                  {
                      title:"任务名称",
                      field: "title",
                      align: "center"
                  },
                  {
                      title:"发起人",
                      field: "initiatorRealname",
                      align: "center"
                  },
                  {
                      title:"负责人",
                      field: "responsiblemanRealname",
                      align: "center"
                  },
                  {
                      title:"执行人",
                      field: "executorRealname",
                      align: "center"
                  },
                  {
                      title:"抄送人",
                      field: "ccRealName"
                  },
                  {
                      title: "任务状态",
                      field: "taskStatusName",
                      align: "center"
                  },
                  {
                      title: "任务优先级",
                      field: "priorityName",
                      align: "center"
                  },
                  {
                      title:"任务类型",
                      field: "taskTypeName",
                      align: "center"
                  },
                  {
                      title:"任务来源",
                      field: "taskSourceName",
                      align: "center"
                  },
                  {field: 'action', title: '操作', align: 'center', formatter: 'action', events:operationEvents ,clickToSelect: false}
              ]
          })
    });

    let operationEvents = {
        "click .view":function (event,value,row,index){
            window.open("${pageContext.servletContext.contextPath}/manage/task/taskinfo?taskId="+row.taskId);
        }
    }

    function action(){
        return "<a href='#' class='view'>查看详细</a>"
    }

</script>
</body>
</html>
