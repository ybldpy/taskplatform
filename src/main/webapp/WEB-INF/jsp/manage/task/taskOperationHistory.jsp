<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: acer
  Date: 2023/2/23
  Time: 21:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>任务操作历史记录</title>
    <c:import url="/resources/inc/head.jsp"/>
    <script src="<c:url value="/resources/admin/plugins/jquery.1.12.4.min.js"/>"></script>
    <script src="<c:url value="/resources/admin/plugins/bootstrap-3.3.0/js/bootstrap.min.js"/>"></script>
    <script src="<c:url value="/resources/admin/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"/>"></script>
    <script src="<c:url value="/resources/admin/plugins/bootstrap-table-1.11.0/bootstrap-table.min.js"/>"></script>
</head>
<body>

<div id="main" style="margin-top: 2%;">
    <div id="searchBar">
        <div class="col-xs-3">
            <input type="text" class="form-control" id="searchText" onchange="search();" placeholder="操作人搜索"/>
        </div>
        <div class="col-xs-3">
            <select class="form-control" id="actionSelector" onchange="search();">
                <option value=""></option>
                <option value="0">其他</option>
                <option value="1">新建任务</option>
                <option value="2">修改任务</option>
                <option value="3">删除任务</option>
                <option value="4">指派人员</option>
                <option value="5">暂停任务</option>
                <option value="6">激活任务</option>
                <option value="7">完成任务</option>
                <option value="8">作废任务</option>
                <option value="9">关闭任务</option>
            </select>
        </div>
        <div class="col-xs-3">
            <input class="form-control" id="dateSelector" type="text" onchange="search();" placeholder="日期搜索" />
        </div>
    </div>

    <div>
        <table id="his-table"></table>
    </div>

</div>
<script>

    let rows = [];

    function getDataFromModelMap(){
        let dataList = [];
        <c:forEach items="${tpTaskOperationChildList}" var="operationHistory">
        dataList.push({
            operationHistoryId:'${operationHistory.id}',
            operator:'${operationHistory.operator}',
            operatorRealName: '${operationHistory.operatorRealName}',
            ctime:${operationHistory.ctime},
            actionType:${operationHistory.actionType},
            actionName:'${operationHistory.actionName}',
            remark:'<c:choose><c:when test="${operationHistory.remark==null}">""</c:when><c:otherwise>${operationHistory.remark}</c:otherwise></c:choose>',
            time:'${operationHistory.time}'
        });
        </c:forEach>
        rows = dataList;
        return dataList;
    }

    $(function (){

        $("#his-table").bootstrapTable({
            toolbar:"#searchBar",
            detailView:true,
            detailFormatter:function (index,row,e){
                return "<textarea class='form-control' style='resize: none' readonly>"+row.remark+"</textarea>";
            },
            data:getDataFromModelMap(),
            columns:[
                {
                    title:"编号",
                    field:"operationHistoryId"
                },
                {
                    title:"操作者",
                    field: "operatorRealName"
                },
                {
                    title:"操作类型",
                    field: "actionName"
                },
                {
                    title:"操作时间",
                    field: "time"
                },
                {
                    field: "remark",
                    visible:false
                }
            ]
        });
        $('#dateSelector').datetimepicker({
            language:"zh-CN",
            clearBtn:1,
            autoclose:1,
            todayHighlight:1,
            format:"yyyy-mm-dd",
            startView: 2,
            minView: 2,
        });

    });



    function search(){
        let operator = $("#searchText").val();
        let actionType = $("#actionSelector").val();
        let date = $("#dateSelector").val();

        let $table = $("#his-table");

            for(let i=0;i<rows.length;i++){

                if ((rows[i]["operatorRealName"].includes(operator))&&(actionType==""||rows[i]["actionType"]==actionType)&&(rows[i]["time"]==date||date=="")){
                    $table.bootstrapTable("showRow",{index:i});
                }
                else {
                    $table.bootstrapTable("hideRow",{index: i});
                }
            }

    }

</script>
</body>
</html>
