<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: 远坂凛的男朋友
  Date: 2023/2/28
  Time: 20:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>附件</title>
</head>
<body>
<c:import url="/resources/inc/head.jsp"/>
<c:import url="/resources/inc/footer.jsp"/>
<script src="<c:url value="/resources/admin/plugins/spark-md5.min.js"/>"></script>
<div class="main" style="margin-top: 2%">
    <div class="fileInputContainer" style="width: 90%">
            <div class="col-xs-6"><input type="file" class="form-control" id="fileInput" name="files" multiple/></div>
            <div class="col-xs-6"><button type="button" id="fileBtn" class="btn btn-primary">上传</button></div>
    </div>
    <div style="width: 90%">
        <table id="file-table" class="table table-form">
            <thead>
            <th>文件ID</th>
            <th>文件名</th>
            <th>操作</th>
            </thead>
            <tbody id="t-body">
            <c:forEach items="${files}" var="file">
                <tr>
                    <td>${file.fd}</td>
                    <td>${file.fileName}</td>
                    <td><a href="${pageContext.servletContext.contextPath}/manage/task/file/download?fd=${file.fd}">下载</a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>

<script>

    $(function (){
        $("#fileBtn").on("click",function (){
            let files = $("#fileInput")[0].files;
            if (files.length == 0){
                $.alert({title:"",content:"请选择文件"});
                return;
            }
            let body = $("#t-body");
            for(let i=0;i<files.length;i++){
                let tr = document.createElement("tr");
                tr.innerHTML = "<td>-1</td><td>"+files[i].name+"</td><td>0</td>"
                body.prepend(tr)
                uploadFile(files[i],tr);
            }
        });

    });
    function uploadFile(file,tr){
        $.ajax(
            {
                url:'${pageContext.servletContext.contextPath}/manage/task/file/getUploadPlan',
                type:'get',
                data:{
                    associationTaskId:${taskId},
                    fileName:file.name,
                    fileSize:file.size
                },
                success: function (res){
                    if (res.code!=1){
                        //stop
                        showTryAgainBtn(file,tr);
                    }
                    else {
                        $(tr).children("td")[0].innerText = res.data.fd;
                        doUpload(file,res.data,0,tr);
                    }
                },
                error:function (){
                  showTryAgainBtn(file,tr);
                }
            }
        )
    }
    function setTrId(fileName,fd){

        $("#file-"+fileName)[0].id = "fd-"+fd;
    }
    function showTryAgainBtn(file,tr){
        if ($(tr).children("button").length>0){
            return;
        }
        let td = $(tr).children("td")[2];
        td.innerHTML = "";
        let btn = document.createElement("button");
        td.append(btn);
        btn.className = "btn btn-danger";
        btn.innerText = "重试";
        btn.onclick = function (){
            td.remove(this);
            uploadFile(file,tr);
        }
    }



     async function createFormData(file,chunkSize,chunkIndex,fd){
        let formData = new FormData();
        formData.append("fd",fd);
        formData.append("index",chunkIndex);
        let blob = sliceFile(file,chunkSize,chunkIndex);
        await getMd5(blob,formData);
        formData.append("fileData",blob);
        return formData;

    }
    function sliceFile(file,chunkSize,index){
        let fileSize = file.size;
        let start = chunkSize*index;
        let end = start+chunkSize;
        if (end>fileSize+1){
            end = fileSize+1;
        }
        let blob = file.slice(start,end);
        return blob;
    }

    function getMd5(blob,formDate){
        let fileReader = new FileReader();
        let bytes = fileReader.readAsArrayBuffer(blob);
        let spark = new SparkMD5.ArrayBuffer();
        spark.append(bytes);
        return spark.end();
    }

    <%--function reUpload(file,chunkSize,chunkIndex,fd){--%>
    <%--    $.ajax(--%>
    <%--        {--%>
    <%--            url: '${pageContext.servletContext.contextPath}/manage/task/file/upload',--%>
    <%--            type: "post",--%>
    <%--            data:createFormData(file,chunkSize,chunkIndex,fd),--%>
    <%--            processData:false,--%>
    <%--            contentType:false,--%>
    <%--            success:function (res){--%>
    <%--                if (res.code!=1){--%>
    <%--                    //服务器内部错误--%>
    <%--                    showTryAgainBtn(file,fd);--%>
    <%--                }--%>
    <%--                else {--%>
    <%--                    if (res.data!=200){--%>
    <%--                        reUpload(file,chunkSize,chunkIndex,fd);--%>
    <%--                    }--%>
    <%--                    else {--%>
    <%--                        //update progress--%>
    <%--                        updateProgress();--%>
    <%--                    }--%>
    <%--                }--%>
    <%--            },--%>
    <%--            error:function (){--%>
    <%--                showTryAgainBtn(file,function (){--%>
    <%--                    let tr = $("#fd-"+fd);--%>
    <%--                    tr[0].id = "file-"+file.fileName;--%>
    <%--                    return tr.children("td")[2];--%>
    <%--                });--%>
    <%--            }--%>
    <%--        }--%>
    <%--    )--%>
    <%--}--%>

    function updateProgress(index,total,tr,fd){
        let tds = $(tr).children("td");

        if (index == total-1){
            tds[2].innerHTML = "<a href='${pageContext.servletContext.contextPath}/manage/task/file/download?fd="+fd+">下载</a>";
            return;
        }
        tds[2].innerText = Math.round(index/total)+"%";
    }
    async function doUpload(file,plan,chunkIndex,tr){
        let formData = new FormData();
        formData.append("fd",plan.fd);
        formData.append("index",chunkIndex);
        let blob = sliceFile(file,plan.chunkSize,chunkIndex);
        let fileReader = new FileReader();
        formData.append("fileData",blob);
        fileReader.onerror = function (ev){
            alert(ev);
        }
        fileReader.onload = function(e){
            let spark = new SparkMD5.ArrayBuffer();
            spark.append(e.target.result);
            let md5 = spark.end();
            formData.append("md5",md5);
            $.ajax(
                {
                    url: '${pageContext.servletContext.contextPath}/manage/task/file/upload',
                    type: "post",
                    data: formData,
                    processData:false,
                    contentType:false,
                    success:function (res){
                        if (res.code!=1){
                            //服务器内部错误
                            showTryAgainBtn(file,tr);
                        }
                        else {
                            if (res.data!=200){
                                doUpload(file,plan,chunkIndex,tr);
                            }
                            else {
                                //update progress
                                updateProgress(chunkIndex,plan.chunkCount,tr,plan.fd);
                                if (chunkIndex+1<plan.chunkCount){
                                doUpload(file,plan,chunkIndex+1,tr);
                                }
                            }
                        }
                    },
                    error:function (){
                        showTryAgainBtn(file,tr);
                    }
                }
            );
        };
        fileReader.readAsArrayBuffer(blob);
            <%--$.ajax(--%>
            <%--    {--%>
            <%--        url: '${pageContext.servletContext.contextPath}/manage/task/file/upload',--%>
            <%--        type: "post",--%>
            <%--        data: await createFormData(file,plan.chunkSize,chunkIndex,plan.fd),--%>
            <%--        processData:false,--%>
            <%--        contentType:false,--%>
            <%--        success:function (res){--%>
            <%--            if (res.code!=1){--%>
            <%--                //服务器内部错误--%>
            <%--                showTryAgainBtn(file,tr);--%>
            <%--            }--%>
            <%--            else {--%>
            <%--                if (res.data!=200){--%>
            <%--                    doUpload(file,plan,chunkIndex,tr);--%>
            <%--                }--%>
            <%--                else {--%>
            <%--                    //update progress--%>
            <%--                    updateProgress(chunkIndex,plan.chunkCount,tr);--%>
            <%--                    doUpload(file,plan,chunkIndex+1,tr);--%>
            <%--                }--%>
            <%--            }--%>
            <%--        },--%>
            <%--        error:function (){--%>
            <%--            showTryAgainBtn(file,tr);--%>
            <%--        }--%>
            <%--    }--%>
            <%--)--%>

    }

</script>
</body>
</html>
