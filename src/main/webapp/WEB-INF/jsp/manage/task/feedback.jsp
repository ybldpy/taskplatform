<%--
  Created by IntelliJ IDEA.
  User: acer
  Date: 2023/2/21
  Time: 13:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://shiro.apache.org/tags" prefix="shiro"%>
<html>
<head>
    <title>任务反馈</title>
</head>
<body>
<div class="main" style="margin-top: 2%;margin-left: 2%">
    <div class="searchContainer" style="width: 15%">
        <input class="form-control" placeholder="日期搜索" type="text" id = "dateSelector"/>
        <c:import url="/resources/inc/head.jsp"/>
        <c:import url="/resources/inc/footer.jsp"/>
        <script>
            $(function (){
                $('#dateSelector').datetimepicker({
                    language:"zh-CN",
                    clearBtn:1,
                    autoclose:1,
                    todayHighlight:1,
                    format:"yyyy-mm-dd",
                    startView: 2,
                    minView: 2,
                });

                $("#dateSelector")[0].onchange = function (){

                    if (this.value == "" || this.value == undefined || this.value == null){
                        let rows = $("#feedback-table").children("tbody").children("tr");
                        for(let i=0;i<rows.length;i++){
                            if (rows[i].style.display=='none' && rows[i].className=="feedback-info"){
                                rows[i].style.display = 'table-row';
                            }
                            else if (rows[i].className=="feedback-feedback"){
                                rows[i].style.display = 'none';
                            }
                        }
                    }
                    else {
                        let feedbackTimes = $(".feedback-time");
                        for(let i=0;i<feedbackTimes.length;i++){
                            let parent = $(feedbackTimes[i]).parent();
                            alert(feedbackTimes[i].innerText)
                            if (feedbackTimes[i].innerText!=this.value){
                                parent.hide();
                                parent.next().hide();
                            }
                            else {
                                parent[0].style.display = 'table-row';
                            }
                        }
                    }
                }
            })
        </script>
    </div>
    <div style="width: 90%">
        <table id = "feedback-table" class="table table-form" style="margin-top: 1%">
            <thead>
            <th>反馈号</th>
            <th>反馈日期</th>
            <th>操作</th>
            </thead>
            <tbody>
            <c:forEach items="${feedbacks}" var="feedback">
                <tr style="<c:if test="${feedback.status==0}">background:darkgray</c:if>" class="feedback-info">
                    <td onclick="showFeedback('feedback-${feedback.feedbackId}')">${feedback.feedbackId}</td>
                    <td onclick="showFeedback('feedback-${feedback.feedbackId}')" class="feedback-time">${feedback.time}</td>
                    <td><c:choose><c:when test="${feedback.status==1}"><a href="#" onclick="abandon(${feedback.feedbackId})">作废</a> </c:when><c:otherwise>已作废</c:otherwise></c:choose></td>
                </tr>
                <tr style="display: none;height: 150px" id="feedback-${feedback.feedbackId}" class="feedback-feedback">
                    <th>反馈内容</th>
                    <td colspan="2">
                        <textarea class="form-control" style="resize: none;height: 145px" readonly>${feedback.feedback}</textarea>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
<script>

    function abandon(feedbackId){
             $.confirm(
                 {   content:"确认废弃",
                     buttons:{
                          confirm:{
                              text:"确认",
                              action:function (){
                                  $.ajax(
                                      {
                                          url:"${pageContext.servletContext.contextPath}/manage/task/feedback/abandon?feedbackId="+feedbackId,
                                          type:"get",
                                          success:function (result){
                                              if (result.code!=1){
                                                  $.alert({
                                                      title:"失败",
                                                      content:result.message | result.data
                                                  })
                                              }
                                              else {
                                                  window.location.href = "${pageContext.servletContext.contextPath}/manage/task/result?message=成功";
                                              }
                                          },
                                          error:function (){
                                              $.alert({
                                                  content:"服务器错误"
                                              })
                                          }
                                      }
                                  )
                              }
                          },
                          cancel:{
                              text: "取消"
                          }
                     }
                 }
             )
    }

    function showFeedback(feedbackRowId){
               let tr = $('#'+feedbackRowId)[0];
               if (tr.style.display=='none'){
                   tr.style.display = 'table-row';
               }
               else
               {
                   tr.style.display = 'none';
               }
    }
</script>
</body>
</html>
