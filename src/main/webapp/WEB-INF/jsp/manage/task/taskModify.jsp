<%--
  Created by IntelliJ IDEA.
  User: acer
  Date: 2023/2/17
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://shiro.apache.org/tags" prefix="shiro"%>

<html>
<head>
    <title>任务修改-${tpTaskChild.title}</title>
    <link href="<c:url value="/resources/admin/plugins/bootstrap-3.3.0/css/bootstrap.min.css" />" rel="stylesheet"/>
    <link href="<c:url value="/resources/admin/plugins/font-awesome-4.7.0/css/font-awesome.min.css" />" rel="stylesheet"/>
    <link href="<c:url value="/resources/admin/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>" rel="stylesheet"/>
    <link href="<c:url value="/resources/admin/plugins/jquery-confirm/jquery-confirm.min.css"/>" rel="stylesheet"/>
    <script src="<c:url value="/resources/admin/plugins/jquery.1.12.4.min.js"/>"></script>
    <script src="<c:url value="/resources/admin/plugins/jquery.cookie.js"/>"></script>
    <script src="<c:url value="/resources/admin/plugins/bootstrap-3.3.0/js/bootstrap.min.js"/>"></script>
    <script src="<c:url value="/resources/admin/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js" />"></script>
    <script src="<c:url value="/resources/admin/plugins/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js" />"></script>
    <script src="<c:url value="/resources/admin/js/mySelect.js"/>"></script>
    <script src="<c:url value="/resources/admin/plugins/jquery-confirm/jquery-confirm.min.js"/>"></script>

    <style type="text/css">
        div.menuSideBar { }
        div.menuSideBar li.nav-header { font-size: 14px; padding: 3px 15px; }
        div.menuSideBar .nav-list > li > a, div.menuSideBar .dropdown-menu li a { -webkit-border-radius: 0px; -moz-border-radius: 0px; -ms-border-radius: 0px; border-radius: 0px; }
        .background{
            width: 100%;
            height: 100%;
            background: #000;
            position: fixed;
            display: none;
            top: 0;
            left: 0;
            z-index: 10;
        }
        .selectDialog{
            position: relative;
            margin: auto;
            background: #ffffff;
            display: block;
            width: 50%;
            height: 50%;
            top: 20%;
            z-index: 10;
        }
        li:hover{
            background: #5bc0de;
            cursor: pointer;
        }
        label:hover{cursor: pointer}
    </style>

</head>
<body>
<script type="text/javascript">
    $(document).ready(function (){
        $('.tasks-item-checkbox').click(
            function (){
                selectItem(this,$('#selectedTaskItems')[0]);
            }
        )
        $('.systems-item-checkbox').click(
            function (){
                selectItem(this,$('#selectedSystemItems')[0])
            }
        )
        $('.tasks-item').click(function (){
                $(this).children("input")[0].click();
            }
        )

        $('.systems-item').click(
            function (){
                $(this).children("input")[0].click();
            }
        )
    })
</script>
<div class="panel panel-default">
    <div class="background" style="display: none" id="selectTaskDialogBackground">
        <div class="selectDialog" id="selectTaskDialog">
            <div style="height: 60%;width: 100%" class="upper" >
                <div style="float: left;height: 100%;width: 50%">
                    <div class="row">
                        <div class="col-xs-3"></div>

                        <div class="col-xs-12">
                            <div style="float: left"><input class="form-control" type="text" placeholder="search" id="task_search" oninput="inputChange(this,$('.tasks-item'))"/></div>
                            <div><button type="button" class="btn btn-default" onclick="search($('#task_search')[0],$('.tasks-item'))">搜索</button></div>
                        </div>
                        <div class="col-xs-3"></div>
                        <div class="col-xs-3"></div>
                        <div class="col-xs-3"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <ul class="list-group" name="asd" id="taskList" style="overflow-y: scroll;height: 80%">
<%--                                <li class="list-group-item tasks-item">--%>
<%--                                    <input type="checkbox" value = 'taskA' id='task-item-1' class="form-check-input me-1 tasks-item-checkbox" />--%>
<%--                                    <label class="form-check-label">a</label>--%>
<%--                                </li>--%>
<%--                                <li class="list-group-item tasks-item">--%>
<%--                                    <input  type="checkbox" value="taskBB" id='task-item-2' class="form-check-input me-1 tasks-item-checkbox"/>--%>
<%--                                    <label class="form-check-label">bb</label>--%>
<%--                                </li>--%>
<%--                                <li class="list-group-item tasks-item">--%>
<%--                                    <input type="checkbox" value="taskCC" id='task-item-3' class="form-check-input me-1 tasks-item-checkbox"/>--%>
<%--                                    <label class="form-check-label">cc</label>--%>
<%--                                </li>--%>
                                <c:forEach items="${tasks}" var="task">
                                    <li class="list-group-item tasks-item">
                                        <input type="checkbox" value="${task.title}" id="task-item-${task.taskId}" class="form-check-input me-1 tasks-item-checkbox">
                                        <label class="form-check-label">${task.title}</label>
                                    </li>
                                </c:forEach>
                            </ul>
                        </div>
                    </div>

                </div>
                <div style="margin-left: 5%;float: left;height: 100%;width: 40%;">
                    <div class="row">
                        <div class="col-xs-12">
                            <label for="selectedTaskItems">已选</label>
                            <ul class="list-group" id="selectedTaskItems" style="height: 80%;overflow-y: scroll;" >
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12" style="text-align: center">
                    <button class="btn btn-default" onclick="confirm('task')">确认</button>
                    <button class="btn btn-default" onclick="cancel('task')">取消</button>
                </div>
            </div>

        </div>
    </div>

    <div class="background" id="selectSystemDialogBackground">
        <div class="selectDialog" id="selectSystemDialog">
            <div style="height: 60%;width: 100%" class="upper" >
                <div style="float: left;height: 100%;width: 50%">
                    <div class="row">
                        <div class="col-xs-3"></div>

                        <div class="col-xs-12">
                            <div style="float: left"><input class="form-control" type="text" placeholder="search" id="system_search" oninput="inputChange(this,$('.systems-item'))"/></div>
                            <div><button type="button" class="btn btn-default" onclick="search($('#system_search')[0],$('.systems-item'))">搜索</button></div>
                        </div>
                        <div class="col-xs-3"></div>
                        <div class="col-xs-3"></div>
                        <div class="col-xs-3"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <ul class="list-group" name="asd" id="systemList" style="overflow-y: scroll;height: 80%">
<%--                                <li class="list-group-item systems-item">--%>
<%--                                    <input type="checkbox" value="systemA" id='system-item-1' class="form-check-input me-1 systems-item-checkbox" />--%>
<%--                                    <label class="form-check-label">a</label>--%>
<%--                                </li>--%>
<%--                                <li class="list-group-item systems-item">--%>
<%--                                    <input  type="checkbox" value="systemBB" id='system-item-2' class="form-check-input me-1 systems-item-checkbox"/>--%>
<%--                                    <label class="form-check-label">bb</label>--%>
<%--                                </li>--%>
<%--                                <li class="list-group-item systems-item">--%>
<%--                                    <input type="checkbox" value="systemCC" id='system-item-3' class="form-check-input me-1 systems-item-checkbox"/>--%>
<%--                                    <label class="form-check-label">cc</label>--%>
<%--                                </li>--%>
                                <c:forEach items="${systems}" var="system">
                                    <li class="list-group-item systems-item">
                                        <input type="checkbox" value = "${system.title}" class="form-check-input me-1 systems-item-checkbox" id="system-item-${system.systemId}"/>
                                        <label class="form-check-label">${system.title}</label>
                                    </li>
                                </c:forEach>
                            </ul>
                        </div>
                    </div>

                </div>
                <div style="margin-left: 5%;float: left;height: 100%;width: 40%;">
                    <div class="row">
                        <div class="col-xs-12">
                            <label for="selectedSystemItems">已选</label>
                            <ul class="list-group" id="selectedSystemItems" style="overflow: scroll;height: 80%" >
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12" style="text-align: center">
                    <button class="btn btn-default" onclick="confirm('system');">确认</button>
                    <button class="btn btn-default" onclick="cancel('system');">取消</button>
                </div>
            </div>

        </div>
    </div>
    <form class="form-condensed" id="modifyForm">
        <table class="table table-form">
            <input type="text" value="${tpTaskChild.taskId}" name="taskId" hidden=""/>
            <tr>
                <th>任务类型</th>
                <td><div class="row">
                    <div class="col-xs-3">
                        <select id="task_type" name="taskType" class="form-control">
                            <option value="0" <c:if test="${tpTaskChild.taskType==0}">selected</c:if>>系统运维</option>
                            <option value="1" <c:if test="${tpTaskChild.taskType==1}">selected</c:if>>系统升级</option>
                            <option value="2" <c:if test="${tpTaskChild.taskType==2}">selected</c:if>>应急演练</option>
                            <option value="3" <c:if test="${tpTaskChild.taskType==3}">selected</c:if> >其他</option>
                        </select>
                    </div>
                    <div class="col-xs-3"></div>
                    <div class="col-xs-3"></div>
                    <div class="col-xs-3"></div>
                </div>
                </td>
            </tr>
            <tr>
                <th>
                    优先级
                </th>
                <td>
                    <div class="row">
                      <div class="col-xs-3">
                        <select name="priority" class="form-control">
                            <option value="0" <c:if test="${tpTaskChild.priority==0}">selected</c:if>>底</option>
                            <option value="1" <c:if test="${tpTaskChild.priority==1}">selected</c:if>>中</option>
                            <option value="2" <c:if test="${tpTaskChild.priority==2}">selected</c:if>>高</option>
                        </select>
                          <div class="col-xs-3"></div>
                          <div class="col-xs-3"></div>
                          <div class="col-xs-3"></div>
                     </div>
                    </div>
                </td>
            </tr>
            <tr>
                <th>
                    关联任务
                </th>
                <td>
                    <div class="row">
                        <div class="col-xs-3">
                            <input type="text" name="taskAssociation" id="associatedTask" value="${tpTaskChild.taskAssociation}" hidden="hidden"/>
                            <input type="text" class="form-control" id="associatedTaskShowValue" value="<c:forEach items="${tasks}" var="task"><c:if test="${associatedTaskIds.contains(task.taskId)}">${task.title},</c:if></c:forEach>" onclick="showSelectionDialog('task');"/>
                        </div>
                        <div class="col-xs-3"></div>
                        <div class="col-xs-3"></div>
                        <div class="col-xs-3"></div>
                    </div>
                </td>
            </tr>
            <tr>
                <th>
                    关联系统
                </th>
                <td>
                    <div class="row">
                        <div class="col-xs-3">
                            <input type="text" name="systemAssociation" id="associatedSystem" value="${tpTaskChild.systemAssociation}" hidden="hidden"/>
                            <input type="text" class="form-control" id="associatedSystemShowValue" value="<c:forEach items="${systems}" var="system"><c:if test="${associatedSystemIds.contains(system.systemId)}">${system.title},</c:if></c:forEach>" onclick="showSelectionDialog('system');"/>
                        </div>
                        <div class="col-xs-3"></div>
                        <div class="col-xs-3"></div>
                        <div class="col-xs-3"></div>
                    </div>
                </td>
            </tr>

            <tr>
                <th>任务名称</th>
                <td>
                    <div class="row">
                        <div class="col-xs-3">
                            <input type="text" name="title" class="form-control" value="${tpTaskChild.title}" id="task_title">
                        </div>
                        <div class="col-xs-3"></div>
                        <div class="col-xs-3"></div>
                        <div class="col-xs-3"></div>
                    </div>
                </td>
            </tr>
            <tr>
                <th>任务描述</th>
                <td>
                    <div class="row">
                        <div class="col-xs-12">
                            <textarea class="form-control" rows="10" value="${tpTaskChild.description}" name="description">${tpTaskChild.description}</textarea>
                        </div>
                        <div class="col-xs-3"></div>
                        <div class="col-xs-3"></div>
                        <div class="col-xs-3"></div>
                    </div>
                </td>
            </tr>
            <tr>
                <th>汇报周期</th>
                <td>
                    <div class="row">
                        <div class="col-xs-3">
                            <select name="reportingCycle" class="form-control">
                                <option value="0" <c:if test="${tpTaskChild.reportingCycle==0}">selected</c:if>>每天</option>
                                <option value="1" <c:if test="${tpTaskChild.reportingCycle==1}">selected</c:if>>每三天</option>
                                <option value="2" <c:if test="${tpTaskChild.reportingCycle==2}">selected</c:if>>每周</option>
                                <option value="3" <c:if test="${tpTaskChild.reportingCycle==3}">selected</c:if>>每月</option>
                            </select>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <th>备注</th>
                <td>
                    <div class="row">
                        <div class="col-xs-12">
                            <textarea rows="10" class="form-control" name="remark"></textarea>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <th>开始日期</th>
                <td>
                    <div class="row">
                        <div class="col-xs-3 input-group date form_date" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_starttime" data-link-format="yyyy-mm-dd">
                            <input class="form-control" size="16" type="text" value="${tpTaskChild.showStarttime}" readonly>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            <input type="hidden" id="dtp_starttime" value="${tpTaskChild.showStarttime}" class="form-control" name='showStarttime'/><br/>
                        </div>
                        <div class="col-xs-3"></div>
                        <div class="col-xs-3"></div>
                        <div class="col-xs-3"></div>
                    </div>
                </td>
            </tr>
            <tr>
                <th>截止日期</th>
                <td><div class="row">
                    <div class="col-xs-3 input-group date form_date" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_endtime" data-link-format="yyyy-mm-dd">
                        <input class="form-control" size="16" type="text" value="${tpTaskChild.showEndtime}" readonly>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                        <input type="hidden" id="dtp_endtime" value="${tpTaskChild.showEndtime}" class="form-control" name='showEndtime'/><br/>
                    </div>
                    <div class="col-xs-3"></div>
                    <div class="col-xs-3"></div>
                    <div class="col-xs-3"></div>
                </div>
                </td>
            </tr>
        </table>
        <center><button type="button" onclick="doModify()" class="btn btn-default">修改</button></center>
    </form>
</div>
<script type="text/javascript">

    $('.form_date').datetimepicker({
        language: 'zh-CN',
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });

    function doModify(){

        $.ajax(
            {
                type:"post",
                url:"${pageContext.servletContext.contextPath}/manage/task/taskModify",
                data:$("#modifyForm").serialize(),
                beforeSend:function (){
                    //截至日期必须晚于开始日期
                    let diff = new Date($('#dtp_endtime')[0].value).getTime()-new Date($('#dtp_starttime')[0].value).getTime();
                    if (diff<0){
                        $.alert(
                            {
                                content:"截止日期不能早于开始日期!"
                            }
                        )
                        return false;
                    }
                    let title = $('#task_title')[0].value;
                    if (title==null||title==''){
                        $.alert(
                            {
                                content:"任务标题不能为空!"
                            }
                        )
                        return false;
                    }
                    return true;
                },
                success:function (result){
                    if (result.code!=1){
                        $.alert({
                            title:"修改失败",
                            content:result.data | result.message
                        })
                    }
                    else{
                        window.location.href = "${pageContext.servletContext.contextPath}/manage/task/result=修改成功!";
                    }
                },
                error:function (XMLHttpRequest, textStatus, errorThrown){
                    $.alert({
                        content:textStatus
                    })
                }
            }
        )

    }
</script>
</body>
</html>
