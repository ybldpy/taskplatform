<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://shiro.apache.org/tags" prefix="shiro"%>
<c:set var="basePath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE HTML>
<html lang="zh-cn">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>任务信息-${tpTaskChild.taskId}</title>
        <link href="<c:url value="/resources/admin/plugins/bootstrap-3.3.0/css/bootstrap.min.css" />" rel="stylesheet"/>
        <link href="<c:url value="/resources/admin/plugins/font-awesome-4.7.0/css/font-awesome.min.css" />" rel="stylesheet"/>
        <link href="<c:url value="/resources/admin/plugins/bTabs/b.tabs.css" />" rel="stylesheet"/>
        <link href="<c:url value="/resources/admin/plugins/personselect/css/common.css" />" rel="stylesheet"/>
        <link href="<c:url value="/resources/admin/plugins/personselect/css/select.css" />" rel="stylesheet"/>
        <link href="<c:url value="/resources/admin/plugins/jquery-confirm/jquery-confirm.min.css" />" rel="stylesheet"/>



        <style type="text/css">
            div.menuSideBar { }
            div.menuSideBar li.nav-header { font-size: 14px; padding: 3px 15px; }
            div.menuSideBar .nav-list > li > a, div.menuSideBar .dropdown-menu li a { -webkit-border-radius: 0px; -moz-border-radius: 0px; -ms-border-radius: 0px; border-radius: 0px; }
            #assignBackground {
                opacity: 0.8;
                width: 100%;
                height: 100%;
                background: #000;
                position: fixed;
                top: 0;
                left: 0;
                display: none;
                z-index: 2;
            }
            .operationDialog{
                width: 35%;
                height: 40%;
                background: #fff;
                display: none;
                z-index: 2;
                position: fixed;
                top: 30%;
                left: 28%;
                text-align: center;
            }

            #successDialog{
                height: 20%;
                width: 25%;
                display: none;
                background: #fff;
                z-index: 2;
                position: fixed;
                top: 30%;
                left: 30%;
                text-align: center;
            }
        </style>
    </head>
    <body>

        <div class="content">
            <div class="container">
                <h3 class="page-header" <c:if test="${feedbackExpired==null}">style="color: red" </c:if>>任务标题：${tpTaskChild.title}<c:if test="${feedbackExpired==null}">(反馈超过汇报周期)</c:if></h3>

                <div id="assignBackground"></div>
                <div id="assignContent" class="operationDialog">


                    <form method="post" id="assignWorkerForm">
                        <input type="hidden" name="taskId" value="${tpTaskChild.taskId}">
                        <table class="table table-form">
                            <tr>
                                <th>责任人<span style="color:red">*</span></th>
                                <td><div class="row">
                                    <div class="col-xs-3">
                                        <input type='text' name='showresponsibleman' id='area_btn_ysingle-responsibleman' value='${tpTaskChild.responsiblemanRealname}' onclick="show_lay('single-responsibleman')" class='form-control' autocomplete='off' />
                                        <input id="nruid_ysingle-responsibleman" type="hidden" class="form-control" name="responsibleMan" value="${tpTaskChild.responsibleman}">
                                    </div>
                                    <div class="col-xs-3"></div>
                                    <div class="col-xs-3"></div>
                                    <div class="col-xs-3"></div>
                                </div>
                                </td>
                            </tr>
                            <tr>
                                <th>执行人</th>
                                <td><div class="row">
                                    <div class="col-xs-6">
                                        <input type='text' name='showexecutor' id='area_btn_yexecutor' value='${tpTaskChild.executorRealname}' onclick="show_lay('executor')" class='form-control' autocomplete='off' />
                                        <input id="nruid_yexecutor" type="hidden" class="form-control" name="executor" value="${tpTaskChild.executor}">
                                    </div>
                                    <div class="col-xs-6"></div>
                                </div>
                                </td>

                            </tr>
                            <tr>
                                <th>备注</th>
                                <td>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <input type="text" name="remark" style="height: 38%"/>
                                        </div>
                                        <div class="col-xs-6"></div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><div class="row"><div class="col-xs-6"><button type="button" onclick="submitAssignForm()" class="btn btn-danger">指派</button></div></div></td><td><div class="row"> <div class="col-xs-6"><button type="button" onclick="hideAssignDialog()" class="btn btn-blue">取消</button></div></div></td>
                            </tr>
                        </table>
                    </form>




                </div>

                <div id="dialog" class="operationDialog">

                    <form method="post" id="operation_form" class="form-condensed">
                        <input type="hidden" name="taskId" value="${tpTaskChild.taskId}">
                            <label for="remarkArea" class="label-info">
                                备注
                            </label>
                            <textarea class="form-control" id="remarkArea" name="remark" style="max-width: 100%;" rows="10"></textarea>

                    </form>
                    <div style="float: left;position: relative;top: 5px;"><div style="float: left;margin-right: 5px"><button id="submitOperationBtn" class="btn btn-danger">确认</button></div><div style="float: left;"></div><button onclick="hideDialog()" class="btn btn-blue">取消</button></div>
                </div>

                <div id="successDialog">
                    <span>操作成功</span>
                    <div class="row">
                        <div class="col-xs-3"></div>
                        <div class="col-xs-3"></div>
                        <div class="col-xs-3"></div>
                    </div>
                    <div class="row">
                    <button class="btn-danger" onclick="location.reload()">确定</button>
                    </div>
                </div>



                <nav class="navbar navbar-default" role="navigation">
                    <div class="container-fluid">                        
                        <div>                            
                            <form class="navbar-form navbar-right" role="search">
                                <c:if test="${tpTaskChild.initiator.equals(currentUser)}"><button type="button" class="btn btn-default" onclick="showAssignDialog()">
                                    指派人员
                                </button>
                                </c:if>
                                <c:if test="${tpTaskChild.summary!=null&&tpTaskChild.initiator.equals(currentUser)&&tpTaskChild.taskStatus==0}"><button type="button" class="btn btn-default" onclick="showDialog('${basePath}/manage/task/complete')">
                                    完成任务
                                </button></c:if>
                                <c:if test="${(tpTaskChild.taskStatus==1||tpTaskChild.taskStatus==3)&&(tpTaskChild.initiator.equals(currentUser)||admin)}"><button type="button" class="btn btn-default" onclick="showDialog('${basePath}/manage/task/close')">
                                    关闭任务
                                </button></c:if>
                                <c:if test="${tpTaskChild.taskStatus==2||tpTaskChild.taskStatus==0}"><button type="button" class="btn btn-default" onclick="showDialog('${basePath}/manage/task/stop')">
                                    暂停任务
                                </button></c:if>
                                <c:if test="${tpTaskChild.taskStatus==2||tpTaskChild.taskStatus==3}"><button type="button" class="btn btn-default" onclick="showDialog('${basePath}/manage/task/active')">
                                    激活任务
                                </button></c:if>
                                <c:if test="${tpTaskChild.taskStatus==0||tpTaskChild.taskStatus==2}"><button type="button" class="btn btn-default" onclick="showDialog('${basePath}/manage/task/cancel')">
                                    作废任务
                                </button></c:if>
                                <c:if test="${tpTaskChild.taskStatus!=1&&tpTaskChild.taskStatus!=4&&tpTaskChild.initiator.equals(currentUser)}"><button type="button" class="btn btn-default" onclick="window.location.href = '${basePath}/manage/task/taskModify?taskId=${tpTaskChild.taskId}'">
                                    修改任务
                                </button></c:if>
                                <c:if test="${admin}"><button type="button" class="btn btn-default" onclick="showDialog('${basePath}/manage/task/delete')">
                                    删除任务
                                </button>
                                </c:if>
                            </form>




                            <script type="text/javascript">


                                function operation(url){
                                    $.ajax(
                                        {
                                            url:url,
                                            type: "post",
                                            data: $("#operation_form").serialize(),
                                            success:function (result){
                                                if (result.code!=1){
                                                    alert(result.data);
                                                }
                                                else {
                                                    document.getElementById("dialog").style.display = 'none';
                                                    document.getElementById("successDialog").style.display='block';

                                                }
                                            },
                                            error:function (XMLHttpRequest){
                                                console.log(XMLHttpRequest);
                                                alert(XMLHttpRequest.status)
                                            }
                                        }
                                    )
                                }





                                function showDialog(url){
                                    document.getElementById("assignBackground").style.display = 'block';
                                    document.getElementById('dialog').style.display = 'block';
                                    document.getElementById("submitOperationBtn").onclick = function (){

                                        operation(url);
                                    };
                                }
                                function hideDialog(){
                                    document.getElementById('assignBackground').style.display = 'none';
                                    document.getElementById('dialog').style.display = 'none';
                                    document.getElementById('submitOperationBtn').onclick = function (){
                                        document.getElementById("remarkArea").value = '';
                                    };

                                }

                                function showAssignDialog(){
                                    document.getElementById("assignBackground").style.display='block';
                                    document.getElementById("assignContent").style.display='block';

                                }
                                function hideAssignDialog(){
                                    document.getElementById("assignBackground").style.display='none';
                                    document.getElementById("assignContent").style.display='none';

                                }
                                function submitAssignForm(){
                                    $.ajax({
                                            type:"post",
                                            url:"${basePath}/manage/task/assignWorker",
                                            data: $('#assignWorkerForm').serialize(),
                                            beforeSend: function () {
                                                if ($('#nruid_ysingle-responsibleman').val() == '') {
                                                    $('#nruid_ysingle-responsibleman').focus();
                                                    alert("责任人不能为空！")
                                                    return false;
                                                }
                                            },
                                            success:function (result){
                                                if (result == null ||result.code!=1){
                                                    alert('修改失败');
                                                }
                                                else {
                                                    window.location.href = "${basePath}/manage/task/result?message=保存成功！";
                                                }

                                            }

                                        }

                                    )
                                }
                            </script>

                        </div>
                    </div>
                </nav>
                <div class="">
                    <div class="row-fluid">
                        <div class="col-md-2" style="padding-left: 0px;">
                            <div class="well menuSideBar" style="padding: 8px 0px;">
                                <ul class="nav nav-list" id="menuSideBar">
                                    <li class="nav-header">相关信息</li>
                                    <li class="nav-divider"></li>
                                    <li mid="tab1" funurl="<c:url value="/manage/task/taskAssociation?taskId=${tpTaskChild.taskId}" />"><a tabindex="-1" href="javascript:void(0);">相关任务</a></li>
                                    <li mid="tab2" funurl="<c:url value="/manage/task/result?message=页面完善中" />"><a tabindex="-1" href="javascript:void(0);">相关服务器</a></li>
                                    <li mid="tab3" funurl="<c:url value="/manage/task/result?message=页面完善中" />"><a tabindex="-1" href="javascript:void(0);">相关系统</a></li>
                                    <li mid="tab4" funurl="<c:url value="/manage/task/feedback/get?taskId=${tpTaskChild.taskId}" />"><a tabindex="-1" href="javascript:void(0);">反馈</a></li>
                                    <li mid="tab4" funurl="<c:url value="/manage/task/operationHistory/get?taskId=${tpTaskChild.taskId}" />"><a tabindex="-1" href="javascript:void(0);">任务历史记录</a></li>
                                    <li mid="tab4" funurl="<c:url value="/manage/task/file/filePage?taskId=${tpTaskChild.taskId}" />"><a tabindex="-1" href="javascript:void(0);">任务附件</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-10" id="mainFrameTabs" style="padding : 0px;">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active noclose"><a href="#bTabs_navTabsMainPage" data-toggle="tab">基本信息</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="bTabs_navTabsMainPage">
                                    <div id='wrap'>
                                        <div class='content_wrap'>
                                            <div class='panel panel-default'>                   
                                                <form class='form-condensed' method='post' id='addtaskform'>                    
                                                    <table class='table table-form'>  
                                                        <tr>
                                                            <th>任务编号</th>
                                                            <td><div class="row">
                                                                    <div class="col-xs-3">
                                                                        ${tpTaskChild.taskId}
                                                                    </div>
                                                                    <div class="col-xs-3"></div>
                                                                    <div class="col-xs-3"></div>
                                                                    <div class="col-xs-3"></div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>任务来源</th>
                                                            <td><div class="row">
                                                                    <div class="col-xs-3">
                                                                        ${tpTaskChild.taskSourceName}
                                                                    </div>
                                                                    <div class="col-xs-3"></div>
                                                                    <div class="col-xs-3"></div>
                                                                    <div class="col-xs-3"></div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>任务类型</th>
                                                            <td><div class="row">
                                                                    <div class="col-xs-3">
                                                                        ${tpTaskChild.taskTypeName}
                                                                    </div>
                                                                    <div class="col-xs-3"></div>
                                                                    <div class="col-xs-3"></div>
                                                                    <div class="col-xs-3"></div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>任务状态</th>
                                                            <td><div class="row">
                                                                    <div class="col-xs-3">
                                                                        ${tpTaskChild.taskStatusName}
                                                                    </div>
                                                                    <div class="col-xs-3"></div>
                                                                    <div class="col-xs-3"></div>
                                                                    <div class="col-xs-3"></div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>优先级</th>
                                                            <td><div class="row">
                                                                    <div class="col-xs-2">
                                                                        ${tpTaskChild.priorityName}
                                                                    </div>
                                                                    <div class="col-xs-2"></div>
                                                                    <div class="col-xs-2"></div>
                                                                    <div class="col-xs-2"></div>
                                                                    <div class="col-xs-2"></div>
                                                                    <div class="col-xs-2"></div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>发起人</th>
                                                            <td><div class="row">
                                                                    <div class="col-xs-3">                                           
                                                                        ${tpTaskChild.initiatorRealname}
                                                                    </div>
                                                                    <div class="col-xs-3"></div>
                                                                    <div class="col-xs-3"></div>
                                                                    <div class="col-xs-3"></div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>责任人</th>
                                                            <td><div class="row">
                                                                    <div class="col-xs-3">                                           
                                                                        ${tpTaskChild.responsiblemanRealname}
                                                                    </div>
                                                                    <div class="col-xs-3"></div>
                                                                    <div class="col-xs-3"></div>
                                                                    <div class="col-xs-3"></div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>执行人</th>
                                                            <td><div class="row">
                                                                    <div class="col-xs-6">                                           
                                                                        ${tpTaskChild.executorRealname}
                                                                    </div>
                                                                    <div class="col-xs-6"></div>                                      
                                                                </div>
                                                            </td>
                                                        </tr>  
                                                        <tr>
                                                            <th>抄送人</th>
                                                            <td><div class="row">
                                                                    <div class="col-xs-6">                                           
                                                                        ${tpTaskChild.ccRealname}
                                                                    </div>
                                                                    <div class="col-xs-6"></div>                                      
                                                                </div>
                                                            </td>
                                                        </tr> 
                                                        <tr>
                                                            <th>任务名称</th>
                                                            <td><div class="row">
                                                                    <div class="col-xs-12">                                           
                                                                        ${tpTaskChild.title}
                                                                    </div>                                                                            
                                                                </div>
                                                            </td>
                                                        </tr>      
                                                        <tr>
                                                            <th>任务描述</th>
                                                            <td><div class="row">
                                                                    <div class="col-xs-12">                                           
                                                                        <textarea name='description' id='description' rows='10' class='form-control' readonly="yes">${tpTaskChild.description}</textarea>
                                                                    </div>                                                                            
                                                                </div>
                                                            </td>
                                                        </tr>   
                                                        <tr>
                                                            <th>开始日期</th>
                                                            <td><div class="row">
                                                                    <div class="col-xs-3 input-group date form_date" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_starttime" data-link-format="yyyy-mm-dd">
                                                                        ${tpTaskChild.showStarttime}
                                                                    </div>
                                                                    <div class="col-xs-3"></div>
                                                                    <div class="col-xs-3"></div>
                                                                    <div class="col-xs-3"></div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>截止日期</th>
                                                            <td><div class="row">
                                                                    <div class="col-xs-3 input-group date form_date" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_endtime" data-link-format="yyyy-mm-dd">
                                                                        ${tpTaskChild.showEndtime}
                                                                    </div>
                                                                    <div class="col-xs-3"></div>
                                                                    <div class="col-xs-3"></div>
                                                                    <div class="col-xs-3"></div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <div class="row">
                                                                <td colspan="5">
                                                                    <div class="col-xs-12">
                                                                        <label>情况反馈</label>
                                                                        <textarea class="form-control" name="feedback" rows="10" style="resize: none"></textarea>
                                                                        <div class="form-check">
                                                                            <input class="form-check-input" type="checkbox" id="feedback-show-up" checked>
                                                                            <label class="form-check-label" for="feedback-show-up">
                                                                                在周报显示
                                                                            </label>
                                                                        </div>
                                                                        <div style="margin-top: 5px">
                                                                            <button class="btn-primary btn" type="button" id="feedbackSubmitBtn">提交</button>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </div>
                                                        </tr>

                                                        <tr>
                                                            <div class="row">
                                                                <td colspan="5">
                                                                    <div class="col-xs-12">
                                                                        <label>情况总结</label>
                                                                        <textarea class="form-control" name="summary" rows="10" style="resize: none">${tpTaskChild.summary}</textarea>
                                                                        <div style="margin-top: 5px">
                                                                            <c:if test="${true}"><button class="btn-primary btn" type="button" id="summarySubmitBtn"><c:choose><c:when test='${tpTaskChild.summary.equals("") || tpTaskChild.summary==null}'>提交</c:when><c:otherwise>更改</c:otherwise></c:choose></button></c:if>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </div>
                                                        </tr>
                                                    </table>                                                     
                                                </form>
                                            </div>
                                        </div>                
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <c:import url="/resources/inc/personselect.jsp" />

        <c:import url="/resources/inc/footer.jsp" />
        <script src="<c:url value="/resources/admin/plugins/personselect/js/select.js" />"></script>
        <script type="text/javascript" src="<c:url value="/resources/admin/plugins/bTabs/b.tabs.js" />" ></script> 
        <script type="text/javascript" src="<c:url value="/resources/admin/plugins/bTabs/demo.js" />" ></script>
    <script>
        $(function (){
            $('#summarySubmitBtn').click(function (){
                $.ajax({
                    url:"${pageContext.servletContext.contextPath}/manage/task/postSummary",
                    type:"post",
                    data:getSummaryFormData(),
                    beforeSend:function (){
                        if ($("textarea[name='summary']").val()==""){
                            $.alert({title:"",content:"情况总结为空"});
                            return false;
                        }
                        return true;
                    },
                    success:function (res){
                        if (res.code!=1){
                            $.alert({
                                title:"",
                                content:res.data|res.message
                            })
                        }
                        else {
                            $.alert({
                                title:"",
                                content: res.message | '成功'
                            })
                        }
                    },
                    error:function (XMLHttpRequest, textStatus, errorThrown){
                        $.alert({
                            title:"",
                            content:textStatus
                        })
                    }
                });
            });

            $("#feedbackSubmitBtn").click(function (){
                $.ajax({
                    url:"${pageContext.servletContext.contextPath}/manage/task/feedback/post",
                    type:"post",
                    data:getFeedbackFormData(),
                    beforeSend:function (){
                        if ($("textarea[name='feedback']").val()==""){
                            $.alert({title:"",content:"情况总结为空"});
                            return false;
                        }
                        return true;
                    },
                    success:function (res){
                        if (res.code!=1){
                            $.alert({
                                title:"",
                                content:res.message
                            })
                        }
                        else {
                            $.alert({
                                content: res.message
                            })
                        }
                    },
                    error:function (XMLHttpRequest, textStatus, errorThrown){
                        $.alert({
                            content:textStatus
                        })
                    }
                });
            })
        })

        function getSummaryFormData(){
            let data = {};
            data.summary = $("textarea[name='summary']").val();
            data.taskId = ${tpTaskChild.taskId};
            return data;
        }
        function getFeedbackFormData(){
            let data = {};
            data.feedback=$("textarea[name='feedback']").val();
            let showUp = $("#feedback-show-up")[0].checked;
            let showUpNumber = 1;
            if (showUp==null||showUp==undefined){
            }
            else {
                if (!showUp){
                    showUpNumber = 0;
                }
            }
            data.taskAssociationId = ${tpTaskChild.taskId};
            data.showUp = showUpNumber;
            return data;
        }
    </script>
    </body>
</html>