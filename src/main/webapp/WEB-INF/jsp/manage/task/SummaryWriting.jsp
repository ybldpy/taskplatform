<%--
  Created by IntelliJ IDEA.
  User: acer
  Date: 2023/2/24
  Time: 21:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<html>
<head>
    <title>情况总结</title>
    <c:import url="/resources/inc/head.jsp"/>
    <script src='<c:url value="/resources/admin/plugins/jquery.1.12.4.min.js"/>'></script>
    <script src='<c:url value="/resources/admin/plugins/bootstrap-3.3.0"/> '></script>
    <script src='<c:url value="/resources/admin/plugins/jquery-confirm/jquery-confirm.min.js"/> '></script>


</head>
<body>
<div class="main" style="position: fixed;height: 80%;width: 100%;">
    <div id="writingArea">
        <form id="summaryForm">
            <input type="text" value="${taskId}" readonly hidden name="taskId"/>
            <table class="table table-form">
                <tr>
                    <div class="row">
                        <td>
                            <div class="col-xs-12">
                                <label>情况总结</label>
                                <textarea class="form-control" name="summary" rows="10" style="resize: none"></textarea>
                                <div style="margin-top: 5px">
                                    <button class="btn-info btn" type="button" id="submitBtn">提交</button>
                                </div>
                            </div>
                        </td>
                    </div>
                </tr>
            </table>

        </form>
    </div>
</div>
<script>
    $(function (){
        $('#submitBtn').click(function (){
            $.ajax({
                url:"${pageContext.servletContext.contextPath}/manage/task/postSummary",
                type:"post",
                data:$("#summaryForm").serialize(),
                success:function (res){
                    if (res.code!=1){
                        $.alert({
                            title:"上传失败",
                            content:res.data|res.message
                        })
                    }
                    else {
                        $.alert({
                            content: res.message | '成功'
                        })
                    }
                },
                error:function (XMLHttpRequest, textStatus, errorThrown){
                    $.alert({
                        content:textStatus
                    })
                }
            })
        })
    })
</script>
</body>
</html>
