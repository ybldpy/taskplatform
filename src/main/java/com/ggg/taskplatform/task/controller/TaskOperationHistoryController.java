package com.ggg.taskplatform.task.controller;

import com.ggg.taskplatform.task.dao.model.TpTaskOperHistory;
import com.ggg.taskplatform.task.dao.model.TpTaskOperHistoryExample;
import com.ggg.taskplatform.task.dao.model.TpTaskOperationHistoryChild;
import com.ggg.taskplatform.task.operator.TaskOperOperator;
import com.ggg.taskplatform.task.operator.UserOperator;
import com.ggg.taskplatform.task.rpc.api.TpTaskOperHistoryService;
import com.ggg.taskplatform.task.rpc.api.TpTaskService;
import com.magicube.framework.common.utils.FatherToChildUtil;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/manage/task/operationHistory")
public class TaskOperationHistoryController {

    @Autowired
    TpTaskOperHistoryService tpTaskOperHistoryService;

    @Autowired
    TpTaskService tpTaskService;


    @Autowired
    TaskOperOperator taskOperOperator;


    @ApiOperation("任务历史记录的jsp")
    @GetMapping("/get")
    @RequiresPermissions("tp:task:mytodo")
    public String getTaskOperationHistory(@RequestParam(required = false,defaultValue = "0",value = "taskId")int taskId, ModelMap modelMap) throws InvocationTargetException {
        TpTaskOperHistoryExample tpTaskOperHistoryExample = new TpTaskOperHistoryExample();
        tpTaskOperHistoryExample.or().andTaskIdEqualTo(taskId);
        List<TpTaskOperHistory> tpTaskOperHistoryList = tpTaskOperHistoryService.selectByExample(tpTaskOperHistoryExample);
        List<TpTaskOperationHistoryChild> taskOperationHistoryChildList = new ArrayList<>(ObjectUtils.isEmpty(tpTaskOperHistoryList)?1:tpTaskOperHistoryList.size());
        if (!ObjectUtils.isEmpty(tpTaskOperHistoryList)){
            for(TpTaskOperHistory taskOperHistory:tpTaskOperHistoryList){
                TpTaskOperationHistoryChild tpTaskOperationHistoryChild = new TpTaskOperationHistoryChild();
                FatherToChildUtil.fatherToChild(taskOperHistory,tpTaskOperationHistoryChild);
                taskOperOperator.covertChildField(tpTaskOperationHistoryChild);
                taskOperationHistoryChildList.add(tpTaskOperationHistoryChild);
            }
        }

        modelMap.put("tpTaskOperationChildList",taskOperationHistoryChildList);
        return "/manage/task/taskOperationHistory.jsp";
    }
}
