package com.ggg.taskplatform.task.controller;


import com.baidu.unbiz.fluentvalidator.ComplexResult;
import com.baidu.unbiz.fluentvalidator.FluentValidator;
import com.baidu.unbiz.fluentvalidator.ResultCollectors;
import com.ggg.taskplatform.task.constant.TaskOperationConstant;
import com.ggg.taskplatform.task.dao.model.*;
import com.ggg.taskplatform.task.rpc.api.TpTaskFeedbackService;
import com.ggg.taskplatform.task.rpc.api.TpTaskOperHistoryService;
import com.ggg.taskplatform.task.utils.TaskOperationUtils;

import com.ggg.taskplatform.task.utils.TpTaskUtils;
import com.magicube.framework.common.base.BaseController;
import com.magicube.framework.common.constant.UpmsConstant;
import com.magicube.framework.common.constant.UpmsResult;
import com.magicube.framework.common.constant.UpmsResultConstant;
import com.magicube.framework.common.utils.DateFormatUtil;
import com.magicube.framework.common.utils.FatherToChildUtil;
import com.magicube.framework.common.validator.LengthValidator;
import com.magicube.framework.common.validator.NotBlankValidator;
import com.magicube.framework.upms.dao.model.*;
import com.ggg.taskplatform.task.constant.TaskConstant;
import com.ggg.taskplatform.task.operator.TaskOperator;
import com.ggg.taskplatform.task.operator.UserOperator;
import com.ggg.taskplatform.task.rpc.api.TpTaskService;
import com.magicube.framework.upms.rpc.api.UpmsSystemService;
import com.magicube.framework.upms.rpc.api.UpmsUserRoleService;
import com.magicube.framework.upms.rpc.api.UpmsUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.lang.reflect.InvocationTargetException;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javafx.concurrent.Task;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

/**
 * 任务controller
 *
 * @author justincai
 */
@Controller
@Api(value = "任务管理")
@RequestMapping("/manage/task")
public class TaskController extends BaseController {

    private static final Log log = LogFactory.getLog(TaskController.class);

    @Autowired
    private UserOperator userOperator;

    @Autowired
    private TaskOperator taskOperator;

    @Autowired
    private TpTaskService tpTaskService;

    @Autowired
    private UpmsUserRoleService upmsUserRoleService;

    @Autowired
    private TpTaskOperHistoryService tpTaskOperHistoryService;
    @Autowired
    private UpmsSystemService upmsSystemService;
    @Autowired
    private UpmsUserService upmsUserService;
    @Autowired
    private TpTaskFeedbackService tpTaskFeedbackService;


    @RequiresPermissions("tp:task:mytodo")
    @GetMapping("/myInitiationIndex")
    public String myInitiationTask(){
        return "/manage/task/myInitiationIndex.jsp";
    }

    @RequiresPermissions("tp:task:mytodo")
    @GetMapping("/myInitiationList")
    @ResponseBody
    public Map<String,Object> queryMyInitiationTask(@RequestParam(required = false, defaultValue = "0", value = "offset") int offset,
                                                    @RequestParam(required = false, defaultValue = "10", value = "limit") int limit,
                                                    @RequestParam(required = false, defaultValue = "", value = "search") String search,
                                                    HttpServletRequest request) throws InvocationTargetException {
        Subject subject = SecurityUtils.getSubject();
        String userName = (String) subject.getPrincipal();
        TpTaskExample tpTaskExample = new TpTaskExample();

        if (!StringUtils.isBlank(search)){
            if (StringUtils.isNumeric(search)){
                //任务编号
                tpTaskExample.or().andInitiatorEqualTo(userName).andTaskIdEqualTo(Integer.parseInt(search));
            }
            else {
                tpTaskExample.or().andInitiatorEqualTo(userName).andTitleLike("%"+search+"%");
            }
        }
        else {
            tpTaskExample.or().andInitiatorEqualTo(userName);
        }
        int total = tpTaskService.countByExample(tpTaskExample);
        List<TpTask> taskList = tpTaskService.selectByExampleForOffsetPage(tpTaskExample,offset,limit);
        Map<String,Object> map = new HashMap<>();

        if (ObjectUtils.isEmpty(taskList)){
            map.put("rows",null);
            map.put("total",total);
            return map;
        }

        List<TpTaskChild> tpTaskChildren = new ArrayList<>(taskList.size());
        for(TpTask tpTask:taskList){
            TpTaskChild tpTaskChild = new TpTaskChild();
            FatherToChildUtil.fatherToChild(tpTask,tpTaskChild);
            taskOperator.convertTpTaskChildField(tpTaskChild);
            tpTaskChildren.add(tpTaskChild);
        }

        map.put("rows",taskList);
        map.put("total",total);
        return map;




    }

    @ApiOperation(value = "选择联系人")
    @RequestMapping(value = "/personselect", method = RequestMethod.GET)
    public String personselect(ModelMap modelMap) {

        List<UpmsUser> userList = userOperator.getAllUsersExceptAdmin();
        modelMap.put("userList", userList);

        return "/manage/task/personselect.jsp";
    }

    @ApiOperation(value = "新建任务")
    @RequiresPermissions("tp:task:add")
    @RequestMapping(value = "/addtask", method = RequestMethod.GET)
    public String addtask(ModelMap modelMap) {

        //人员选择框数据
        List<UpmsUser> userList = userOperator.getAllUsersExceptAdmin();
        modelMap.put("userList", userList);

        return "/manage/task/addtask.jsp";
    }

    @ApiOperation(value = "新建任务")
    @RequiresPermissions("tp:task:add")
    @ResponseBody
    @RequestMapping(value = "/addtask", method = RequestMethod.POST)
    @Transactional(rollbackFor = Exception.class)
    public Object create(TpTaskChild taskchild) {
        log.debug("taskType:" + taskchild.getTaskType());
        log.debug("priority:" + taskchild.getPriority());
        log.debug("responsibleman:" + taskchild.getResponsibleman());
        log.debug("executor:" + taskchild.getExecutor());
        log.debug("cc:" + taskchild.getCc());
        log.debug("title:" + taskchild.getTitle());
        log.debug("description:" + taskchild.getDescription());
        log.debug("showStarttime:" + taskchild.getShowStarttime());
        log.debug("starttime:" + taskchild.getStarttime());
        log.debug("showEndtime:" + taskchild.getShowEndtime());
        log.debug("endtime:" + taskchild.getEndtime());

        //检查必填项
        ComplexResult result = FluentValidator.checkAll()
                .on(taskchild.getResponsibleman(), new NotBlankValidator("责任人"))
                .on(taskchild.getTitle(), new LengthValidator(1, 1000, "任务名称"))
                .doValidate()
                .result(ResultCollectors.toComplex());
        if (!result.isSuccess()) {
            return new UpmsResult(UpmsResultConstant.FAILED, result.getErrors());
        }

        //检查各字段长度
        result = taskOperator.checkFieldLength(taskchild);
        if (!ObjectUtils.isEmpty(result) && !result.isSuccess()) {
            return new UpmsResult(UpmsResultConstant.INVALID_LENGTH, result.getErrors());
        }

        //检查开始日期与截止日期的合理性（截止日期不能早于开始日期）
        boolean startTimeBeforeEndTime = taskOperator.checkStartAndEndTime(taskchild);
        if (!startTimeBeforeEndTime) {
            return new UpmsResult(UpmsResultConstant.FAILED, "截止日期不能早于开始日期!");
        }

        //得到当前登录用户
        Subject subject = SecurityUtils.getSubject();
        String username = (String) subject.getPrincipal();

        TpTask task = new TpTask();
        task.setTitle(taskchild.getTitle());
        task.setDescription(taskchild.getDescription());
        task.setInitiator(username);    //发起人
        task.setResponsibleman(taskchild.getResponsibleman());
        task.setExecutor(taskchild.getExecutor());
        task.setCc(taskchild.getCc());
        task.setTaskType(taskchild.getTaskType());
        task.setTaskSource(TaskConstant.TASK_SOURCE_SELF);  //自建任务
        task.setPriority(taskchild.getPriority());

        if (!StringUtils.isEmpty(taskchild.getShowStarttime())) {
            Date date = DateFormatUtil.getDateByStringDate(taskchild.getShowStarttime());
            task.setStarttime(date.getTime());
        }

        if (!StringUtils.isEmpty(taskchild.getShowEndtime())) {
            Date date = DateFormatUtil.getDateByStringDate(taskchild.getShowEndtime());
            task.setEndtime(date.getTime());
        }

        task.setTaskStatus(TaskConstant.TASK_STATUS_INPROGRESS);    //任务状态：进行中
        long time = System.currentTimeMillis();
        task.setCtime(time);

        int isSuccess = tpTaskService.insertSelective(task);
        log.info("isSuccess:" + isSuccess);

        if (isSuccess > 0) {
            return new UpmsResult(UpmsResultConstant.SUCCESS, 1);
        } else {
            return new UpmsResult(UpmsResultConstant.FAILED, "保存失败！");
        }

    }

    /**
     * 转入结果页面
     *
     * @param message 显示信息
     * @param modelMap
     * @return
     */
    @ApiOperation(value = "结果页面")
    @RequestMapping(value = "/result", method = RequestMethod.GET)
    public String result(
            @RequestParam(required = true, defaultValue = "", value = "message") String message,
            ModelMap modelMap) {

        modelMap.put("message", message);

        return "/manage/task/result.jsp";
    }

    @ApiOperation(value = "我的待办")
    @RequiresPermissions("tp:task:mytodo")
    @RequestMapping(value = "/mytodoindex", method = RequestMethod.GET)
    public String mytodoindex() {
        return "/manage/task/mytodoindex.jsp";
    }

    @ApiOperation(value = "我的待办列表")
    @RequiresPermissions("tp:task:mytodo")
    @RequestMapping(value = "/mytodolist", method = RequestMethod.GET)
    @ResponseBody
    public Object mytodolist(
            @RequestParam(required = false, defaultValue = "0", value = "offset") int offset,
            @RequestParam(required = false, defaultValue = "10", value = "limit") int limit,
            @RequestParam(required = false, defaultValue = "", value = "search") String search,
            HttpServletRequest request) throws InvocationTargetException {

        //得到当前登录用户
        Subject subject = SecurityUtils.getSubject();
        String username = (String) subject.getPrincipal();

        //待办条件：任务的责任人和执行人中有操作者，任务未处于“已关闭”或“已作废”状态      
        TpTaskExample tpTaskExample = new TpTaskExample();

        //搜索条件
        if (StringUtils.isNotBlank(search)) {
            log.info("search:" + search);
            tpTaskExample.or()
                    .andTaskStatusNotEqualTo(TaskConstant.TASK_STATUS_CLOSED)
                    .andTaskStatusNotEqualTo(TaskConstant.TASK_STATUS_ABANDONED)
                    .andResponsiblemanLike("%" + username + ",%")
                    .andTitleLike("%" + search + "%");      //任务名称
            tpTaskExample.or()
                    .andTaskStatusNotEqualTo(TaskConstant.TASK_STATUS_CLOSED)
                    .andTaskStatusNotEqualTo(TaskConstant.TASK_STATUS_ABANDONED)
                    .andExecutorLike("%" + username + ",%")
                    .andTitleLike("%" + search + "%");      //任务名称

            if (StringUtils.isNumeric(search)) {     //任务编号
                log.info("search str is a number!");
                tpTaskExample.or()
                        .andTaskStatusNotEqualTo(TaskConstant.TASK_STATUS_CLOSED)
                        .andTaskStatusNotEqualTo(TaskConstant.TASK_STATUS_ABANDONED)
                        .andResponsiblemanLike("%" + username + ",%")
                        .andTaskIdEqualTo(Integer.valueOf(search));   //任务编号 
                tpTaskExample.or()
                        .andTaskStatusNotEqualTo(TaskConstant.TASK_STATUS_CLOSED)
                        .andTaskStatusNotEqualTo(TaskConstant.TASK_STATUS_ABANDONED)
                        .andExecutorLike("%" + username + ",%")
                        .andTaskIdEqualTo(Integer.valueOf(search));   //任务编号 
            }
        } else {
            tpTaskExample.or()
                    .andTaskStatusNotEqualTo(TaskConstant.TASK_STATUS_CLOSED)
                    .andTaskStatusNotEqualTo(TaskConstant.TASK_STATUS_ABANDONED)
                    .andResponsiblemanLike("%" + username + ",%");
            tpTaskExample.or()
                    .andTaskStatusNotEqualTo(TaskConstant.TASK_STATUS_CLOSED)
                    .andTaskStatusNotEqualTo(TaskConstant.TASK_STATUS_ABANDONED)
                    .andExecutorLike("%" + username + ",%");
        }

        //排序条件
        tpTaskExample.setOrderByClause(" task_id desc ");

        List<TpTask> tpTaskrows = tpTaskService.selectByExampleForOffsetPage(tpTaskExample, offset, limit);

        //TpTask类转换为TpTaskChild,便于数据转换
        List<TpTaskChild> rows = new ArrayList<>();
        if (!ObjectUtils.isEmpty(tpTaskrows)) {
            for (TpTask tpTask : tpTaskrows) {
                TpTaskChild tpTaskChild = new TpTaskChild();
                FatherToChildUtil.fatherToChild(tpTask, tpTaskChild);

                //转换TpTaskChild对象中相关字段信息
                tpTaskChild = taskOperator.convertTpTaskChildField(tpTaskChild);

                rows.add(tpTaskChild);
            }
        }

        long total = rows.size();

        Map<String, Object> result = new HashMap<>();
        result.put("rows", rows);
        result.put("total", total);
        return result;
    }

    @ApiOperation(value = "我的已办")
    @RequiresPermissions("tp:task:mytodo")
    @RequestMapping(value = "/mydoneindex", method = RequestMethod.GET)
    public String mydoneindex() {
        return "/manage/task/mydoneindex.jsp";
    }

    @ApiOperation(value = "我的已办列表")
    @RequiresPermissions("tp:task:mytodo")
    @RequestMapping(value = "/mydonelist", method = RequestMethod.GET)
    @ResponseBody
    public Object mydonelist(
            @RequestParam(required = false, defaultValue = "0", value = "offset") int offset,
            @RequestParam(required = false, defaultValue = "10", value = "limit") int limit,
            @RequestParam(required = false, defaultValue = "", value = "search") String search,
            HttpServletRequest request) throws InvocationTargetException {

        //得到当前登录用户
        Subject subject = SecurityUtils.getSubject();
        String username = (String) subject.getPrincipal();

        //已办条件：任务的责任人和执行人中没有操作者，但在经办人历史序列，任务未处于“已关闭”或“已作废”状态
        TpTaskExample tpTaskExample = new TpTaskExample();

        //搜索条件
        if (StringUtils.isNotBlank(search)) {
            log.info("search:" + search);
            tpTaskExample.or()
                    .andTaskStatusNotEqualTo(TaskConstant.TASK_STATUS_CLOSED)
                    .andTaskStatusNotEqualTo(TaskConstant.TASK_STATUS_ABANDONED)
                    .andResponsiblemanNotLike("%" + username + ",%")
                    .andExecutorNotLike("%" + username + ",%")
                    .andOperatorlistLike("%" + username + ",%")
                    .andTitleLike("%" + search + "%");      //任务名称

            if (StringUtils.isNumeric(search)) {     //任务编号
                log.info("search str is a number!");
                tpTaskExample.or()
                        .andTaskStatusNotEqualTo(TaskConstant.TASK_STATUS_CLOSED)
                        .andTaskStatusNotEqualTo(TaskConstant.TASK_STATUS_ABANDONED)
                        .andResponsiblemanNotLike("%" + username + ",%")
                        .andExecutorNotLike("%" + username + ",%")
                        .andOperatorlistLike("%" + username + ",%")
                        .andTaskIdEqualTo(Integer.valueOf(search));   //任务编号

            }
        } else {
            tpTaskExample.or()
                    .andTaskStatusNotEqualTo(TaskConstant.TASK_STATUS_CLOSED)
                    .andTaskStatusNotEqualTo(TaskConstant.TASK_STATUS_ABANDONED)
                    .andResponsiblemanNotLike("%" + username + ",%")
                    .andExecutorNotLike("%" + username + ",%")
                    .andOperatorlistLike("%" + username + ",%");

        }

        //排序条件
        tpTaskExample.setOrderByClause(" task_id desc ");

        List<TpTask> tpTaskrows = tpTaskService.selectByExampleForOffsetPage(tpTaskExample, offset, limit);

        //TpTask类转换为TpTaskChild,便于数据转换
        List<TpTaskChild> rows = new ArrayList<>();
        if (!ObjectUtils.isEmpty(tpTaskrows)) {
            for (TpTask tpTask : tpTaskrows) {
                TpTaskChild tpTaskChild = new TpTaskChild();
                FatherToChildUtil.fatherToChild(tpTask, tpTaskChild);

                //转换TpTaskChild对象中相关字段信息
                tpTaskChild = taskOperator.convertTpTaskChildField(tpTaskChild);

                rows.add(tpTaskChild);
            }
        }

        long total = rows.size();

        Map<String, Object> result = new HashMap<>();
        result.put("rows", rows);
        result.put("total", total);
        return result;
    }

    @ApiOperation(value = "我的办结")
    @RequiresPermissions("tp:task:mytodo")
    @RequestMapping(value = "/mycompletedindex", method = RequestMethod.GET)
    public String mycompletedindex() {
        return "/manage/task/mycompletedindex.jsp";
    }

    @ApiOperation(value = "我的办结列表")
    @RequiresPermissions("tp:task:mytodo")
    @RequestMapping(value = "/mycompletedlist", method = RequestMethod.GET)
    @ResponseBody
    public Object mycompletedlist(
            @RequestParam(required = false, defaultValue = "0", value = "offset") int offset,
            @RequestParam(required = false, defaultValue = "10", value = "limit") int limit,
            @RequestParam(required = false, defaultValue = "", value = "search") String search,
            HttpServletRequest request) throws InvocationTargetException {

        //得到当前登录用户
        Subject subject = SecurityUtils.getSubject();
        String username = (String) subject.getPrincipal();

        //办结条件：任务已经处于“已关闭”或“已作废”状态，操作者在经办人历史序列或发起人或责任人或执行人中
        TpTaskExample tpTaskExample = new TpTaskExample();

        //搜索条件
        if (StringUtils.isNotBlank(search)) {
            log.info("search:" + search);
            //“已关闭”状态下
            tpTaskExample.or()
                    .andTaskStatusEqualTo(TaskConstant.TASK_STATUS_CLOSED)
                    .andInitiatorEqualTo(username)
                    .andTitleLike("%" + search + "%");      //任务名称
            tpTaskExample.or()
                    .andTaskStatusEqualTo(TaskConstant.TASK_STATUS_CLOSED)
                    .andResponsiblemanLike("%" + username + ",%")
                    .andTitleLike("%" + search + "%");      //任务名称
            tpTaskExample.or()
                    .andTaskStatusEqualTo(TaskConstant.TASK_STATUS_CLOSED)
                    .andExecutorLike("%" + username + ",%")
                    .andTitleLike("%" + search + "%");      //任务名称
            tpTaskExample.or()
                    .andTaskStatusEqualTo(TaskConstant.TASK_STATUS_CLOSED)
                    .andOperatorlistLike("%" + username + ",%")
                    .andTitleLike("%" + search + "%");      //任务名称

            //“已作废”状态下
            tpTaskExample.or()
                    .andTaskStatusEqualTo(TaskConstant.TASK_STATUS_ABANDONED)
                    .andInitiatorEqualTo(username)
                    .andTitleLike("%" + search + "%");      //任务名称
            tpTaskExample.or()
                    .andTaskStatusEqualTo(TaskConstant.TASK_STATUS_ABANDONED)
                    .andResponsiblemanLike("%" + username + ",%")
                    .andTitleLike("%" + search + "%");      //任务名称
            tpTaskExample.or()
                    .andTaskStatusEqualTo(TaskConstant.TASK_STATUS_ABANDONED)
                    .andExecutorLike("%" + username + ",%")
                    .andTitleLike("%" + search + "%");      //任务名称
            tpTaskExample.or()
                    .andTaskStatusEqualTo(TaskConstant.TASK_STATUS_ABANDONED)
                    .andOperatorlistLike("%" + username + ",%")
                    .andTitleLike("%" + search + "%");      //任务名称

            if (StringUtils.isNumeric(search)) {     //任务编号
                log.info("search str is a number!");
                //“已关闭”状态下
                tpTaskExample.or()
                        .andTaskStatusEqualTo(TaskConstant.TASK_STATUS_CLOSED)
                        .andInitiatorEqualTo(username)
                        .andTaskIdEqualTo(Integer.valueOf(search));   //任务编号
                tpTaskExample.or()
                        .andTaskStatusEqualTo(TaskConstant.TASK_STATUS_CLOSED)
                        .andResponsiblemanLike("%" + username + ",%")
                        .andTaskIdEqualTo(Integer.valueOf(search));   //任务编号
                tpTaskExample.or()
                        .andTaskStatusEqualTo(TaskConstant.TASK_STATUS_CLOSED)
                        .andExecutorLike("%" + username + ",%")
                        .andTaskIdEqualTo(Integer.valueOf(search));   //任务编号
                tpTaskExample.or()
                        .andTaskStatusEqualTo(TaskConstant.TASK_STATUS_CLOSED)
                        .andOperatorlistLike("%" + username + ",%")
                        .andTaskIdEqualTo(Integer.valueOf(search));   //任务编号

                //“已作废”状态下
                tpTaskExample.or()
                        .andTaskStatusEqualTo(TaskConstant.TASK_STATUS_ABANDONED)
                        .andInitiatorEqualTo(username)
                        .andTaskIdEqualTo(Integer.valueOf(search));   //任务编号
                tpTaskExample.or()
                        .andTaskStatusEqualTo(TaskConstant.TASK_STATUS_ABANDONED)
                        .andResponsiblemanLike("%" + username + ",%")
                        .andTaskIdEqualTo(Integer.valueOf(search));   //任务编号
                tpTaskExample.or()
                        .andTaskStatusEqualTo(TaskConstant.TASK_STATUS_ABANDONED)
                        .andExecutorLike("%" + username + ",%")
                        .andTaskIdEqualTo(Integer.valueOf(search));   //任务编号
                tpTaskExample.or()
                        .andTaskStatusEqualTo(TaskConstant.TASK_STATUS_ABANDONED)
                        .andOperatorlistLike("%" + username + ",%")
                        .andTaskIdEqualTo(Integer.valueOf(search));   //任务编号                                    

            }
        } else {
            //“已关闭”状态下
            tpTaskExample.or()
                    .andTaskStatusEqualTo(TaskConstant.TASK_STATUS_CLOSED)
                    .andInitiatorEqualTo(username);
            tpTaskExample.or()
                    .andTaskStatusEqualTo(TaskConstant.TASK_STATUS_CLOSED)
                    .andResponsiblemanLike("%" + username + ",%");
            tpTaskExample.or()
                    .andTaskStatusEqualTo(TaskConstant.TASK_STATUS_CLOSED)
                    .andExecutorLike("%" + username + ",%");
            tpTaskExample.or()
                    .andTaskStatusEqualTo(TaskConstant.TASK_STATUS_CLOSED)
                    .andOperatorlistLike("%" + username + ",%");
            //“已作废”状态下
            tpTaskExample.or()
                    .andTaskStatusEqualTo(TaskConstant.TASK_STATUS_ABANDONED)
                    .andInitiatorEqualTo(username);
            tpTaskExample.or()
                    .andTaskStatusEqualTo(TaskConstant.TASK_STATUS_ABANDONED)
                    .andResponsiblemanLike("%" + username + ",%");
            tpTaskExample.or()
                    .andTaskStatusEqualTo(TaskConstant.TASK_STATUS_ABANDONED)
                    .andExecutorLike("%" + username + ",%");
            tpTaskExample.or()
                    .andTaskStatusEqualTo(TaskConstant.TASK_STATUS_ABANDONED)
                    .andOperatorlistLike("%" + username + ",%");

        }

        //排序条件
        tpTaskExample.setOrderByClause(" task_id desc ");

        List<TpTask> tpTaskrows = tpTaskService.selectByExampleForOffsetPage(tpTaskExample, offset, limit);

        //TpTask类转换为TpTaskChild,便于数据转换
        List<TpTaskChild> rows = new ArrayList<>();
        if (!ObjectUtils.isEmpty(tpTaskrows)) {
            for (TpTask tpTask : tpTaskrows) {
                TpTaskChild tpTaskChild = new TpTaskChild();
                FatherToChildUtil.fatherToChild(tpTask, tpTaskChild);

                //转换TpTaskChild对象中相关字段信息
                tpTaskChild = taskOperator.convertTpTaskChildField(tpTaskChild);

                rows.add(tpTaskChild);
            }
        }

        long total = rows.size();

        Map<String, Object> result = new HashMap<>();
        result.put("rows", rows);
        result.put("total", total);
        return result;
    }

    @ApiOperation(value = "我的待阅")
    @RequiresPermissions("tp:task:mytodo")
    @RequestMapping(value = "/myforreadingindex", method = RequestMethod.GET)
    public String myforreadingindex() {
        return "/manage/task/myforreadingindex.jsp";
    }

    @ApiOperation(value = "我的待阅列表")
    @RequiresPermissions("tp:task:mytodo")
    @RequestMapping(value = "/myforreadinglist", method = RequestMethod.GET)
    @ResponseBody
    public Object myforreadinglist(
            @RequestParam(required = false, defaultValue = "0", value = "offset") int offset,
            @RequestParam(required = false, defaultValue = "10", value = "limit") int limit,
            @RequestParam(required = false, defaultValue = "", value = "search") String search,
            HttpServletRequest request) throws InvocationTargetException {

        //得到当前登录用户
        Subject subject = SecurityUtils.getSubject();
        String username = (String) subject.getPrincipal();

        //待阅条件：操作者在抄送人中，任务未处于“已关闭”或“已作废”状态   
        TpTaskExample tpTaskExample = new TpTaskExample();

        //搜索条件
        if (StringUtils.isNotBlank(search)) {
            log.info("search:" + search);
            tpTaskExample.or()
                    .andTaskStatusNotEqualTo(TaskConstant.TASK_STATUS_CLOSED)
                    .andTaskStatusNotEqualTo(TaskConstant.TASK_STATUS_ABANDONED)
                    .andCcLike("%" + username + ",%")
                    .andTitleLike("%" + search + "%");      //任务名称            

            if (StringUtils.isNumeric(search)) {     //任务编号
                log.info("search str is a number!");
                tpTaskExample.or()
                        .andTaskStatusNotEqualTo(TaskConstant.TASK_STATUS_CLOSED)
                        .andTaskStatusNotEqualTo(TaskConstant.TASK_STATUS_ABANDONED)
                        .andCcLike("%" + username + ",%")
                        .andTaskIdEqualTo(Integer.valueOf(search));   //任务编号 

            }
        } else {
            tpTaskExample.or()
                    .andTaskStatusNotEqualTo(TaskConstant.TASK_STATUS_CLOSED)
                    .andTaskStatusNotEqualTo(TaskConstant.TASK_STATUS_ABANDONED)
                    .andCcLike("%" + username + ",%");
        }

        //排序条件
        tpTaskExample.setOrderByClause(" task_id desc ");

        List<TpTask> tpTaskrows = tpTaskService.selectByExampleForOffsetPage(tpTaskExample, offset, limit);

        //TpTask类转换为TpTaskChild,便于数据转换
        List<TpTaskChild> rows = new ArrayList<>();
        if (!ObjectUtils.isEmpty(tpTaskrows)) {
            for (TpTask tpTask : tpTaskrows) {
                TpTaskChild tpTaskChild = new TpTaskChild();
                FatherToChildUtil.fatherToChild(tpTask, tpTaskChild);

                //转换TpTaskChild对象中相关字段信息
                tpTaskChild = taskOperator.convertTpTaskChildField(tpTaskChild);

                rows.add(tpTaskChild);
            }
        }

        long total = rows.size();

        Map<String, Object> result = new HashMap<>();
        result.put("rows", rows);
        result.put("total", total);
        return result;
    }

    @ApiOperation(value = "任务详细信息")
    @RequiresPermissions("tp:task:mytodo")
    @RequestMapping(value = "/taskinfo", method = RequestMethod.GET)
    public String taskinfo(
            @RequestParam(required = true, defaultValue = "0", value = "taskId") int taskId,
            ModelMap modelMap) throws InvocationTargetException {
        log.info("taskId:" + taskId);
        Subject subject = SecurityUtils.getSubject();
        TpTaskExample tpTaskExample = new TpTaskExample();
        tpTaskExample.or()
                .andTaskIdEqualTo(taskId);
        TpTask tpTask = tpTaskService.selectFirstByExample(tpTaskExample);
        if (tpTask!=null&&!(tpTask.getInitiator().equals(subject.getPrincipal())||tpTask.getResponsibleman().contains((String)subject.getPrincipal())||tpTask.getExecutor().contains((String)subject.getPrincipal())||subject.hasRole("admin")||subject.hasRole("super"))){
            return "/403.jsp";
        }
        TpTaskChild tpTaskChild = new TpTaskChild();
        if (!ObjectUtils.isEmpty(tpTask)) {
            FatherToChildUtil.fatherToChild(tpTask, tpTaskChild);
            //转换TpTaskChild对象中相关字段信息
            tpTaskChild = taskOperator.convertTpTaskChildField(tpTaskChild);
            TpTaskFeedbackExample tpTaskFeedbackExample = new TpTaskFeedbackExample();
            tpTaskFeedbackExample.or().andTaskAssociationIdEqualTo(taskId);
            tpTaskFeedbackExample.setOrderByClause("ctime desc");
            TpTaskFeedback tpTaskFeedback = tpTaskFeedbackService.selectFirstByExample(tpTaskFeedbackExample);
            modelMap.put("feedbackExpired",tpTaskFeedback!=null&&System.currentTimeMillis()-tpTaskFeedback.getCtime()>=1000l*60*60*24*TaskConstant.getReportingCycleDay(tpTask.getReportingCycle()));
        }

        //判断目前用户具有哪些操作权限

        modelMap.put("currentUser",subject.getPrincipal());
        modelMap.put("admin",subject.hasRole("admin")||subject.hasRole("super"));
        modelMap.put("tpTaskChild", tpTaskChild);

        //人员表
        List<UpmsUser> userList = userOperator.getAllUsersExceptAdmin();
        modelMap.put("userList", userList);

        return "/manage/task/taskinfo.jsp";
    }

    @ApiOperation(value = "指派人员")
    @RequiresPermissions("tp:task:operate")
    @PostMapping("/assignWorker")
    @ResponseBody
    @Transactional(rollbackFor = Exception.class)
    public Object assignWorker(@ModelAttribute AssignOperation assignOperation) throws InvocationTargetException {


        TpTask tpTask = tpTaskService.selectByPrimaryKey(assignOperation.getTaskId());
        if (tpTask==null){
            return new UpmsResult(UpmsResultConstant.FAILED,"任务不存在");
        }
        //发起人才能修改执行人和负责人
        Subject subject = SecurityUtils.getSubject();
        if (!tpTask.getInitiator().equals(subject.getPrincipal())){
            return new UpmsResult(UpmsResultConstant.NOT_INITIATOR_OR_RESPONSIBLEMAN,"非发起人");
        }
        if (tpTask.getTaskStatus()== TaskConstant.TASK_STATUS_COMPLETED || tpTask.getTaskStatus()==TaskConstant.TASK_STATUS_CLOSED){
            return new UpmsResult(UpmsResultConstant.FAILED,"任务已完成或关闭");
        }

        tpTask.setExecutor(assignOperation.getExecutor());
        tpTask.setResponsibleman(assignOperation.getResponsibleMan());
        tpTask.setOperatorlist(taskOperator.addTaskOperator(tpTask.getOperatorlist(),(String) subject.getPrincipal()));
        int res = tpTaskService.updateByPrimaryKey(tpTask);
        if (res<=0){
            return new UpmsResult(UpmsResultConstant.FAILED,"操作失败");
        }
        TpTaskOperHistory tpTaskOperHistory = TaskOperationUtils.createOperationHistory(tpTask,System.currentTimeMillis(),(String) subject.getPrincipal(),TaskOperationConstant.ASSIGN_WORKER,assignOperation.getRemark());

        res = tpTaskOperHistoryService.insert(tpTaskOperHistory);

        if (res>0){
            return new UpmsResult(UpmsResultConstant.SUCCESS,"success");
        }

        return new UpmsResult(UpmsResultConstant.FAILED,"failed, try again");
    }

    @ApiOperation("暂停任务")
    @RequiresPermissions("tp:task:operate")
    @ResponseBody
    @PostMapping("/stop")
    @Transactional(rollbackFor = Exception.class)
    public Object taskStop(@RequestParam("taskId")int taskId,@RequestParam("remark") String remark){

        TpTask tpTask = tpTaskService.selectByPrimaryKey(taskId);
        if (tpTask==null){
            return new UpmsResult(UpmsResultConstant.FAILED,"任务不存在");
        }
        if (tpTask.getTaskStatus()!=TaskConstant.TASK_STATUS_INPROGRESS){
            return new UpmsResult(UpmsResultConstant.FAILED,"任务不在进行状态");
        }
        Subject subject = SecurityUtils.getSubject();

        String operatorList = taskOperator.addTaskOperator(tpTask.getOperatorlist(),(String) subject.getPrincipal());
        tpTask.setOperatorlist(operatorList);
        tpTask.setTaskStatus(TaskConstant.TASK_STATUS_SUSPENDED);

        tpTaskService.updateByPrimaryKey(tpTask);

        TpTaskOperHistory tpTaskOperHistory = TaskOperationUtils.createOperationHistory(tpTask,System.currentTimeMillis(),(String) subject.getPrincipal(),TaskOperationConstant.STOP_TASK,remark);

        int res = tpTaskOperHistoryService.insert(tpTaskOperHistory);

        if (res>0){
            return new UpmsResult(UpmsResultConstant.SUCCESS,UpmsResultConstant.SUCCESS.getMessage());
        }
        return new UpmsResult(UpmsResultConstant.FAILED,UpmsResultConstant.FAILED.getMessage());
    }


    @ApiOperation("激活任务")
    @PostMapping("/active")
    @RequiresPermissions("tp:task:operate")
    @ResponseBody
    @Transactional(rollbackFor = Exception.class)
    public Object taskActive(@RequestParam("taskId") int taskId, @RequestParam("remark")String remark){

        TpTask tpTask = tpTaskService.selectByPrimaryKey(taskId);
        if (tpTask==null){
            return new UpmsResult(UpmsResultConstant.FAILED,"任务不存在");
        }
        if (tpTask.getTaskStatus()!=TaskConstant.TASK_STATUS_SUSPENDED&&tpTask.getTaskStatus()!=TaskConstant.TASK_STATUS_ABANDONED){
            return new UpmsResult(UpmsResultConstant.FAILED,"the task does not need to active");
        }
        Subject subject = SecurityUtils.getSubject();

        tpTask.setTaskStatus(TaskConstant.TASK_STATUS_INPROGRESS);
        String operatorList = taskOperator.addTaskOperator(tpTask.getOperatorlist(),(String) subject.getPrincipal());
        tpTask.setOperatorlist(operatorList);
        int res = tpTaskService.updateByPrimaryKey(tpTask);
        if (res<=0){
            return new UpmsResult(UpmsResultConstant.FAILED,"操作失败");
        }

        TpTaskOperHistory tpTaskOperHistory = TaskOperationUtils.createOperationHistory(tpTask,System.currentTimeMillis(),(String) subject.getPrincipal(),TaskOperationConstant.ACTIVE_TASK,remark);

        res = tpTaskOperHistoryService.insert(tpTaskOperHistory);

        if (res>0){
            return new UpmsResult(UpmsResultConstant.SUCCESS,UpmsResultConstant.SUCCESS.getMessage());
        }

        return new UpmsResult(UpmsResultConstant.FAILED,UpmsResultConstant.FAILED.getMessage());

    }

    @ApiOperation("作废任务")
    @RequiresPermissions("tp:task:operate")
    @PostMapping("/cancel")
    @ResponseBody
    @Transactional(rollbackFor = Exception.class)
    public Object taskCancel(@RequestParam("taskId") int taskId,@RequestParam("remark")String remark){

        TpTask tpTask = tpTaskService.selectByPrimaryKey(taskId);
        if (tpTask==null){
            return new UpmsResult(UpmsResultConstant.FAILED,"任务不存在");
        }
        if (tpTask.getTaskStatus()!=TaskConstant.TASK_STATUS_INPROGRESS&&tpTask.getTaskStatus()!=TaskConstant.TASK_STATUS_SUSPENDED){
            return new UpmsResult(UpmsResultConstant.FAILED,"task can be canceled when task is in progress or suspended");
        }

        Subject subject = SecurityUtils.getSubject();

        if (tpTask.getInitiator().equals(subject.getPrincipal()) || tpTask.getResponsibleman().equals(subject.getPrincipal()) || tpTask.getResponsibleman().equals((String) subject.getPrincipal()+",")){
            //执行人栏改成发起人，责任人清空
            tpTask.setExecutor(tpTask.getInitiator()+",");
            tpTask.setResponsibleman("");
        }
        else {

              if (tpTask.getExecutor().contains((String)subject.getPrincipal())){
                  tpTask.setExecutor(tpTask.getResponsibleman());
              }
              else {
                  return new UpmsResult(UpmsResultConstant.FAILED,"not initiator, responsible man or executors");
              }
        }

        tpTask.setTaskStatus(TaskConstant.TASK_STATUS_ABANDONED);
        tpTask.setOperatorlist(taskOperator.addTaskOperator(tpTask.getOperatorlist(),(String) subject.getPrincipal()));
        int res = tpTaskService.updateByPrimaryKey(tpTask);
        if (res<=0){
            return new UpmsResult(UpmsResultConstant.FAILED,"操作失败");
        }

        TpTaskOperHistory tpTaskOperHistory = TaskOperationUtils.createOperationHistory(tpTask,System.currentTimeMillis(),(String) subject.getPrincipal(),TaskConstant.TASK_STATUS_ABANDONED,remark);
        res = tpTaskOperHistoryService.insert(tpTaskOperHistory);

        if (res>0){
            return new UpmsResult(UpmsResultConstant.SUCCESS,UpmsResultConstant.SUCCESS.getMessage());
        }
        return new UpmsResult(UpmsResultConstant.FAILED,UpmsResultConstant.FAILED.getMessage());
    }

    @ApiOperation("关闭任务")
    @PostMapping("/close")
    @RequiresPermissions("tp:task:operate")
    @ResponseBody
    @Transactional(rollbackFor = Exception.class)
    public Object taskClose(@RequestParam("taskId") int taskId,@RequestParam("remark")String remark){

        TpTask tpTask = tpTaskService.selectByPrimaryKey(taskId);
        if (tpTask==null){
            return new UpmsResult(UpmsResultConstant.FAILED,"任务不存在");
        }
        Subject subject = SecurityUtils.getSubject();
        if (!tpTask.getInitiator().equals(subject.getPrincipal())&&!subject.hasRole("admin")&&!subject.hasRole("super")){
            return new UpmsResult(UpmsResultConstant.FAILED,"only initiator, admin or super can close task");
        }
        if (tpTask.getTaskStatus()!=TaskConstant.TASK_STATUS_COMPLETED&&tpTask.getTaskStatus()!=TaskConstant.TASK_STATUS_ABANDONED){
            return new UpmsResult(UpmsResultConstant.FAILED,"only completed or abandoned task can be closed");
        }

        tpTask.setTaskStatus(TaskConstant.TASK_STATUS_CLOSED);
        tpTask.setOperatorlist(taskOperator.addTaskOperator(tpTask.getOperatorlist(),(String) subject.getPrincipal()));
        int res = tpTaskService.updateByPrimaryKey(tpTask);

        if (res<=0){
            return new UpmsResult(UpmsResultConstant.FAILED,"操作失败");
        }

        TpTaskOperHistory tpTaskOperHistory = TaskOperationUtils.createOperationHistory(tpTask,System.currentTimeMillis(),(String) subject.getPrincipal(),TaskOperationConstant.CLOSE_TASK,remark);
        res = tpTaskOperHistoryService.insert(tpTaskOperHistory);

        if (res>0){
            return new UpmsResult(UpmsResultConstant.SUCCESS,UpmsResultConstant.SUCCESS.getMessage());
        }
        return new UpmsResult(UpmsResultConstant.FAILED,UpmsResultConstant.FAILED.getMessage());
    }

    @ApiOperation("完成任务")
    @RequiresPermissions("tp:task:operate")
    @PostMapping("/complete")
    @ResponseBody
    @Transactional(rollbackFor = Exception.class)
    public Object taskComplete(@RequestParam("taskId") int taskId,@RequestParam("remark")String remark){

        TpTask tpTask = tpTaskService.selectByPrimaryKey(taskId);
        if (tpTask==null){
            return new UpmsResult(UpmsResultConstant.FAILED,"任务不存在");
        }
        Subject subject = SecurityUtils.getSubject();
        if (tpTask.getTaskStatus()!=TaskConstant.TASK_STATUS_INPROGRESS){
            return new UpmsResult(UpmsResultConstant.FAILED,"task is not in progress");
        }
        if (StringUtils.isBlank(tpTask.getSummary())){
            return new UpmsResult(UpmsResultConstant.FAILED,"情况总结未提交");
        }

        if (tpTask.getInitiator().equals(subject.getPrincipal())){
            tpTask.setExecutor(tpTask.getInitiator()+",");
            tpTask.setResponsibleman("");
        }
        else {
            return new UpmsResult(UpmsResultConstant.FAILED,"只有任务发起人才能完成任务");
        }

        tpTask.setTaskStatus(TaskConstant.TASK_STATUS_COMPLETED);
        tpTask.setOperatorlist(taskOperator.addTaskOperator(tpTask.getOperatorlist(),(String) subject.getPrincipal()));
        int res = tpTaskService.updateByPrimaryKey(tpTask);

        if (res<=0){
            return new UpmsResult(UpmsResultConstant.FAILED,"操作失败");
        }

        TpTaskOperHistory tpTaskOperHistory = TaskOperationUtils.createOperationHistory(tpTask,System.currentTimeMillis(),(String) subject.getPrincipal(),TaskOperationConstant.TASK_DONE,remark);
        res = tpTaskOperHistoryService.insert(tpTaskOperHistory);
        if (res>0){
            return new UpmsResult(UpmsResultConstant.SUCCESS,UpmsResultConstant.SUCCESS.getMessage());
        }
        return new UpmsResult(UpmsResultConstant.FAILED,UpmsResultConstant.FAILED.getMessage());
    }

    @ApiOperation("修改任务")
    @GetMapping("/taskModify")
    @RequiresPermissions("tp:task:operate")
    public String taskModify(@RequestParam("taskId")int taskId, ModelMap modelMap) throws InvocationTargetException {
        TpTask tpTask = tpTaskService.selectByPrimaryKey(taskId);
        if (tpTask == null){
            return "/manage/task/result?message=任务不存在";
        }
        TpTaskChild tpTaskChild = new TpTaskChild();
        Subject subject = SecurityUtils.getSubject();
        String curUserName = (String) subject.getPrincipal();
        if (!tpTask.getInitiator().equals(curUserName)&&(!tpTask.getResponsibleman().equals(curUserName)||tpTask.getResponsibleman().equals(curUserName+","))){
            return "/403.jsp";
        }

        if (!ObjectUtils.isEmpty(tpTask)){
            if (tpTask.getTaskStatus() == TaskConstant.TASK_STATUS_COMPLETED){
                return "/manage/task/result?message=不能修改已完成的任务";
            }
            FatherToChildUtil.fatherToChild(tpTask,tpTaskChild);
            taskOperator.convertTpTaskChildField(tpTaskChild);
        }
        else {
            return "/404.jsp";
        }



        modelMap.put("tpTaskChild",tpTaskChild);
        TpTaskExample tpTaskExample = new TpTaskExample();
        tpTaskExample.or().andTaskIdNotEqualTo(taskId);
        List<TpTask> tpTasks = tpTaskService.selectByExample(tpTaskExample);
        List<TpTaskChild> tpTaskChildList = taskOperator.convertTpTaskChildField(tpTasks);
        modelMap.put("associatedTaskIds",TpTaskUtils.covertIdStringToSet(tpTask.getTaskAssociation()));
        UpmsSystemExample upmsSystemExample = new UpmsSystemExample();
        List<UpmsSystem> systems = upmsSystemService.selectByExample(upmsSystemExample);
        modelMap.put("associatedSystemIds",TpTaskUtils.covertIdStringToSet(tpTask.getSystemAssociation()));
        modelMap.put("tasks",tpTaskChildList);
        modelMap.put("systems",systems);
//        String taskAssociation = modifyOperation.getTaskAssociation();
//        if (!StringUtils.isBlank(taskAssociation)){
//            String[] taskAssociationId = taskAssociation.split(",");
//            List<TpTaskChild> tpTaskChildren = new ArrayList<>(taskAssociationId.length);
//
//            for(int i=0;i<taskAssociationId.length;i++){
//                TpTask associatedTask = tpTaskService.selectByPrimaryKey(Integer.parseInt(taskAssociationId[i]));
//                if (!ObjectUtils.isEmpty(associatedTask)) {
//                    TpTaskChild taskChild = new TpTaskChild();
//                    FatherToChildUtil.fatherToChild(associatedTask, taskChild);
//                    taskOperator.convertTpTaskChildField(modifyOperation);
//                    tpTaskChildren.add(taskChild);
//                }
//            }
//        }

        return "/manage/task/taskModify.jsp";

    }

    @ApiOperation("修改任务")
    @PostMapping("/taskModify")
    @RequiresPermissions("tp:task:operate")
    @ResponseBody
    @Transactional(rollbackFor = Exception.class)
    public Object taskModify(ModifyOperation modifyOperation){

        if (ObjectUtils.isEmpty(Arrays.asList(modifyOperation.getTaskType(),modifyOperation.getTitle(),modifyOperation.getTaskStatus(),modifyOperation.getReportingCycle(),modifyOperation.getPriority()))){
            return new UpmsResult(UpmsResultConstant.FAILED,"参数缺失");
        }

        Subject subject = SecurityUtils.getSubject();
        String curUser = (String) subject.getPrincipal();
        TpTaskChild tempTaskChild = new TpTaskChild();
        TpTask tpTask = tpTaskService.selectByPrimaryKey(modifyOperation.getTaskId());
        if (tpTask == null){
            return new UpmsResult(UpmsResultConstant.FAILED,"404");
        }
        if (tpTask.getTaskStatus() == TaskConstant.TASK_STATUS_COMPLETED){
            return new UpmsResult(UpmsResultConstant.FAILED,"任务已完成");
        }
        if (tpTask.getTaskStatus() == TaskConstant.TASK_STATUS_CLOSED){
            return new UpmsResult(UpmsResultConstant.FAILED,"任务已关闭");
        }
        if (!(tpTask.getResponsibleman().equals(curUser) || tpTask.getResponsibleman().equals(curUser+",") || tpTask.getInitiator().equals(curUser))){
            return new UpmsResult(UpmsResultConstant.FAILED,"only responsible man or initiator is allowed to modify task");
        }
        if (StringUtils.isBlank(modifyOperation.getTitle())){
            return new UpmsResult(UpmsResultConstant.FAILED,"task title cannot be empty");
        }
        tempTaskChild.setDescription(modifyOperation.getDescription());
        ComplexResult result = taskOperator.checkFieldLength(tempTaskChild);
        if (result!=null&&!result.isSuccess()){
            return new UpmsResult(UpmsResultConstant.FAILED,TpTaskUtils.getValidationErrorMessage(result.getErrors()));
        }

        if (!taskOperator.checkStartAndEndTime(modifyOperation.getShowStarttime(),modifyOperation.getShowEndtime())){
            return new UpmsResult(UpmsResultConstant.FAILED,"截止日期不能晚于开始日期");
        }


        tpTask.setTaskType(modifyOperation.getTaskType());
        tpTask.setTitle(modifyOperation.getTitle());
        tpTask.setReportingCycle(modifyOperation.getReportingCycle());
        tpTask.setDescription(modifyOperation.getDescription());
        tpTask.setPriority(modifyOperation.getPriority());
        tpTask.setTaskAssociation(modifyOperation.getTaskAssociation());
        tpTask.setSystemAssociation(modifyOperation.getSystemAssociation());
        tpTask.setServerAssociation(modifyOperation.getServerAssociation());

        if (!StringUtils.isEmpty(modifyOperation.getShowStarttime())) {
            Date date = DateFormatUtil.getDateByStringDate(modifyOperation.getShowStarttime());
            tpTask.setStarttime(date.getTime());
        }

        if (!StringUtils.isEmpty(modifyOperation.getShowEndtime())) {
            Date date = DateFormatUtil.getDateByStringDate(modifyOperation.getShowEndtime());
            tpTask.setEndtime(date.getTime());
        }

        String operatorList = taskOperator.addTaskOperator(tpTask.getOperatorlist(),curUser);
        tpTask.setOperatorlist(operatorList);
        int res = tpTaskService.updateByPrimaryKey(tpTask);
        if (res<=0){
            return new UpmsResult(UpmsResultConstant.FAILED, UpmsResultConstant.FAILED.getMessage());
        }

        TpTaskOperHistory tpTaskOperHistory = TaskOperationUtils.createOperationHistory(tpTask,System.currentTimeMillis(),curUser,TaskOperationConstant.MODIFY_TASK,modifyOperation.getRemark());
        res = tpTaskOperHistoryService.insert(tpTaskOperHistory);
        if(res>0){
            return new UpmsResult(UpmsResultConstant.SUCCESS,UpmsResultConstant.SUCCESS.getMessage());
        }
        return new UpmsResult(UpmsResultConstant.FAILED, UpmsResultConstant.FAILED.getMessage());

    }

    @ApiOperation("关联任务")
    @GetMapping("/taskAssociation")
    @RequiresPermissions("tp:task:operate")
    public String taskAssociation(@RequestParam(required = true,defaultValue = "0",value = "taskId")int taskId,ModelMap modelMap){modelMap.put("taskId",taskId);return "/manage/task/taskAssociation.jsp";}

    @ApiOperation("关联任务链表")
    @GetMapping("/taskAssociationList")
    @RequiresPermissions("tp:task:mytodo")
    @ResponseBody
    public Map<String,Object> taskAssociationList(@RequestParam(required = true,defaultValue = "0", value = "taskId") int taskId,@RequestParam(required = false,value = "search")String search,
    @RequestParam(required = false,value = "offset",defaultValue = "0")int offset,@RequestParam(required = false,value = "limit",defaultValue = "10")int limit) throws InvocationTargetException {

        TpTask tpTask = tpTaskService.selectByPrimaryKey(taskId);
        if (tpTask==null){return TpTaskUtils.createAssociationTasksListResultMap(0,null);}
        Set<Integer> ids = TpTaskUtils.covertIdStringToSet(tpTask.getTaskAssociation());
        if (ids.size()==0){
            return TpTaskUtils.createAssociationTasksListResultMap(0,null);
        }
        TpTaskExample tpTaskExample = new TpTaskExample();
        List<Integer> idList = new ArrayList<>(ids.size());
        for(Integer i:ids){
            idList.add(i);
        }
        tpTaskExample.or().andTaskIdIn(idList);
        //搜索条件
        if (!StringUtils.isBlank(search)){
            tpTaskExample.or().andTitleLike("%"+search+"%");
            UpmsUserExample upmsUserExample = new UpmsUserExample();
            upmsUserExample.or().andRealnameLike("%"+search+"%");
            List<UpmsUser> userList = upmsUserService.selectByExample(upmsUserExample);
            StringBuilder nameStringBuilder = new StringBuilder();
            nameStringBuilder.append("");
            for(UpmsUser upmsUser:userList){
                nameStringBuilder.append(upmsUser.getUsername()).append(",");
            }
            String nameStr = "%"+nameStringBuilder.substring(0,nameStringBuilder.length()-1)+"%";
            tpTaskExample.or().andInitiatorLike(nameStr);
            tpTaskExample.or().andResponsiblemanLike(nameStr);
            tpTaskExample.or().andExecutorLike(nameStr);
            tpTaskExample.or().andCcLike(nameStr);
        }

        int count = tpTaskService.countByExample(tpTaskExample);
        List<TpTask> tpTasks = tpTaskService.selectByExampleForOffsetPage(tpTaskExample,offset,limit);
        List<TpTaskChild> taskChildList = taskOperator.convertTpTaskChildField(tpTasks);
        Map<String,Object> rowstMap = TpTaskUtils.createAssociationTasksListResultMap(count,taskChildList);
        return rowstMap;
    }

    @PostMapping("/postSummary")
    @RequiresPermissions("tp:task:operate")
    @ResponseBody
    @Transactional(rollbackFor = Exception.class)
    public UpmsResult postSummary(@RequestParam(value = "taskId",defaultValue = "0") int taskId,@RequestParam(value = "summary",defaultValue = "") String summary){


        TpTask tpTask = tpTaskService.selectByPrimaryKey(taskId);
        if (ObjectUtils.isEmpty(tpTask)){
            return new UpmsResult(UpmsResultConstant.FAILED,"任务不存在");
        }
        if (StringUtils.isBlank(summary)){
            return new UpmsResult(UpmsResultConstant.FAILED,"情况总结为空");
        }

        Subject subject = SecurityUtils.getSubject();
        if (!tpTask.getInitiator().contains((String)subject.getPrincipal())){
            return new UpmsResult(UpmsResultConstant.FAILED,"只有发起人才能提交情况总结");
        }

        tpTask.setSummary(summary);
        int res = tpTaskService.updateByPrimaryKey(tpTask);
        if (res>0){
            return new UpmsResult(UpmsResultConstant.SUCCESS,UpmsResultConstant.SUCCESS.getMessage());
        }
        return new UpmsResult(UpmsResultConstant.FAILED,UpmsResultConstant.FAILED.getMessage());


    }

}
