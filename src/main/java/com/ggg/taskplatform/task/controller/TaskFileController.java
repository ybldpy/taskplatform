package com.ggg.taskplatform.task.controller;

import com.ggg.taskplatform.task.dao.model.TpTask;
import com.ggg.taskplatform.task.dao.model.TpTaskFile;
import com.ggg.taskplatform.task.dao.model.TpTaskFileExample;
import com.ggg.taskplatform.task.rpc.api.TpTaskService;
import com.ggg.taskplatform.task.utils.MySimpleLocker;
import com.ggg.taskplatform.task.utils.TaskFileUtils;
import com.magicube.framework.common.constant.UpmsResult;
import com.magicube.framework.common.constant.UpmsResultConstant;
import com.magicube.framework.upms.client.shiro.session.UpmsSessionDao;
import io.swagger.models.auth.In;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import com.ggg.taskplatform.task.rpc.api.TpTaskFileService;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.BitSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Controller
@RequestMapping("/manage/task/file")
public class TaskFileController {


    @Autowired
    TpTaskFileService tpTaskFileService;
    @Autowired
    TpTaskService tpTaskService;
    @Autowired
    UpmsSessionDao upmsSessionDao;

    private ConcurrentHashMap<Integer,Map<String,Integer>> fds;

    private MySimpleLocker mapSetLock = new MySimpleLocker();
    private MySimpleLocker incLock = new MySimpleLocker();

    private static final String UPLOADED_COUNT = "uploadedCount";
    private static final String CHUNK_SIZE = "chunkSize";
    private static final String CHUNK_COUNT = "chunkCount";
    private static final int MAX_CHUNK_SIZE = 1024*1024;
    private static final String FILE_STORAGE_BASE = "D:\\taskplatform";



    @GetMapping("/getUploadPlan")
    @RequiresPermissions("tp:task:mytodo")
    @ResponseBody
    @Transactional(rollbackFor = Exception.class)
    public UpmsResult createUploadPlan(@RequestParam(value = "associationTaskId",required = true,defaultValue = "0")int taskId, @RequestParam(value = "fileName",required = true,defaultValue = "")String fileName, @RequestParam(value = "fileSize",required = true,defaultValue = "0")long fileSize){
        if (taskId<=0|| StringUtils.isBlank(fileName)||fileSize<=0){
            return new UpmsResult(UpmsResultConstant.FAILED,UpmsResultConstant.FAILED.getMessage());
        }
        TpTask tpTask = tpTaskService.selectByPrimaryKey(taskId);
        if (ObjectUtils.isEmpty(tpTask)){
            return new UpmsResult(UpmsResultConstant.FAILED,"任务不存在");
        }
        TpTaskFile tpTaskFile = new TpTaskFile();
        tpTaskFile.setAssociationTaskId(taskId);
        tpTaskFile.setFileName(fileName);
        int res = tpTaskFileService.insert(tpTaskFile);
        if (res<=0){
            return new UpmsResult(UpmsResultConstant.FAILED,"创建文件失败");
        }
        String localPath = FILE_STORAGE_BASE+"\\tp_task_file\\"+tpTaskFile.getFd()+"_"+fileName;
        String url = "/manage/task/file/download?fd="+tpTaskFile.getFd();
        String formatFileName = fileName+"_"+tpTaskFile.getFd();
        tpTaskFile.setUrl(url);
        tpTaskFile.setLocalPath(localPath);
        res = tpTaskFileService.updateByPrimaryKey(tpTaskFile);
        if (res<=0){
            tpTaskFileService.deleteByPrimaryKey(tpTaskFile.getFd());
            return new UpmsResult(UpmsResultConstant.FAILED,"创建文件失败");
        }

        File tempFile = null;
        try {
            tempFile = TaskFileUtils.createTempFile(tpTaskFile.getFd()+"_"+fileName,".tmp",new File(FILE_STORAGE_BASE+"\\tp_task_file_temp"),fileSize);
        } catch (IOException e) {
            e.printStackTrace();
            if (tempFile != null && tempFile.exists()) {
                tempFile.delete();
            }
            tpTaskFileService.deleteByPrimaryKey(tpTaskFile.getFd());
            return new UpmsResult(UpmsResultConstant.FAILED,"创建文件失败");
        }

        Map<String,Integer> map = new HashMap<>();
        map.put(CHUNK_SIZE,MAX_CHUNK_SIZE);
        map.put(CHUNK_COUNT,(int) Math.ceil((double) fileSize/MAX_CHUNK_SIZE));
        map.put("fd",tpTaskFile.getFd());
        map.put(UPLOADED_COUNT,0);

        mapSetLock.lock();
        if (ObjectUtils.isEmpty(fds)){
            fds = new ConcurrentHashMap<>();
        }
        mapSetLock.unlock();
        fds.put(tpTaskFile.getFd(),map);
        return new UpmsResult(UpmsResultConstant.SUCCESS,map);
    }


    @PostMapping("/upload")
    @RequiresPermissions("tp:task:mytodo")
    @ResponseBody
    public UpmsResult writeFile(@RequestParam(value = "fd",defaultValue = "-1")int fd, @RequestParam(value = "index",defaultValue = "-1")int index, MultipartFile fileData,
    @RequestParam(value = "md5")String md5){

        if (fd==-1 || index<0){
            return new UpmsResult(UpmsResultConstant.FAILED,"参数错误");
        }

        if (!fds.containsKey(fd)){
            return new UpmsResult(UpmsResultConstant.FAILED,"文件不存在");
        }
        TpTaskFile tpTaskFile = tpTaskFileService.selectByPrimaryKey(fd);
        File tempFile = new File(FILE_STORAGE_BASE+"\\tp_task_file_temp\\"+fd+"_"+tpTaskFile.getFileName()+".tmp");
        if (!tempFile.exists()){
            return new UpmsResult(UpmsResultConstant.FAILED,"文件不存在");
        }

        int chunkCount = fds.get(fd).get("chunkCount");


        if (index>=chunkCount){
            return new UpmsResult(UpmsResultConstant.FAILED,"索引错误");
        }

        byte[] bytes=null;
        try {
            bytes = fileData.getBytes();
        } catch (IOException e) {
            e.printStackTrace();
            return new UpmsResult(UpmsResultConstant.FAILED,"server error");
        }

        if (!TaskFileUtils.getMd5(bytes).equals(md5)){
            return new UpmsResult(UpmsResultConstant.SUCCESS,"mistake data");
        }


        try {
            TaskFileUtils.writeFile(tempFile,index,MAX_CHUNK_SIZE,bytes);
        } catch (IOException e) {
            e.printStackTrace();
            return new UpmsResult(UpmsResultConstant.FAILED,"server error");
        }
        Map<String,Integer> map = fds.get(fd);
        incLock.lock();
        int count = map.get(UPLOADED_COUNT);
        map.put(UPLOADED_COUNT,++count);
        incLock.unlock();


        if (count==chunkCount){
            //可以开启一个异步线程来把文件从temp目录copy到存放目录
            try {
                TaskFileUtils.writeTempFileToFileDir(tempFile,tpTaskFile.getLocalPath());
                tpTaskFile.setUploadStatus(1);
                tpTaskFileService.updateByPrimaryKey(tpTaskFile);
            } catch (IOException e) {
                e.printStackTrace();
                return new UpmsResult(UpmsResultConstant.FAILED,"server error");
            }
        }
        return new UpmsResult(UpmsResultConstant.SUCCESS,200);
    }

    @GetMapping("/filePage")
    @RequiresPermissions("tp:task:mytodo")
    public String getFilePage(@RequestParam(value = "taskId",defaultValue = "0")int taskId, ModelMap modelMap){
        TpTaskFileExample tpTaskFileExample = new TpTaskFileExample();
        tpTaskFileExample.or().andAssociationTaskIdEqualTo(taskId).andUploadStatusEqualTo(1);
        List<TpTaskFile> tpTaskFiles = tpTaskFileService.selectByExample(tpTaskFileExample);
        TaskFileUtils.hideFileLocalPath(tpTaskFiles);
        modelMap.put("files",tpTaskFiles);
        modelMap.put("taskId",taskId);
        return "/manage/task/file.jsp";
    }

    @GetMapping("/download")
    @RequiresPermissions("tp:task:mytodo")
    public void downloadFile(@RequestParam("fd")int fd, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse){
        TpTaskFile tpTaskFile = tpTaskFileService.selectByPrimaryKey(fd);
        if (tpTaskFile == null || tpTaskFile.getUploadStatus()!=1){
            httpServletResponse.setStatus(404);
            return;
        }

        httpServletResponse.setHeader("Content-Disposition","attachment;filename=\""+tpTaskFile.getFileName()+"\"");
        httpServletResponse.setContentType("application/octet-stream");
        OutputStream outputStream = null;
        InputStream inputStream = null;
        FileInputStream fileInputStream = null;
        File file = new File(tpTaskFile.getLocalPath());

        try {
            fileInputStream = new FileInputStream(file);
            inputStream = new BufferedInputStream(fileInputStream);
            outputStream = httpServletResponse.getOutputStream();
            TaskFileUtils.readFileToStream(inputStream,outputStream,MAX_CHUNK_SIZE);
            outputStream.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            httpServletResponse.setStatus(500);
            return;
        } catch (IOException e) {
            e.printStackTrace();
            httpServletResponse.setStatus(500);
            return;
        }
        finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


}
