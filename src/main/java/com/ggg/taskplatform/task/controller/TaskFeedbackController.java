package com.ggg.taskplatform.task.controller;

import com.ggg.taskplatform.task.constant.TaskFeedbackConstant;
import com.ggg.taskplatform.task.dao.model.TpTask;
import com.ggg.taskplatform.task.dao.model.TpTaskFeedback;
import com.ggg.taskplatform.task.dao.model.TpTaskFeedbackChild;
import com.ggg.taskplatform.task.dao.model.TpTaskFeedbackExample;
import com.ggg.taskplatform.task.rpc.api.TpTaskFeedbackService;
import com.ggg.taskplatform.task.rpc.api.TpTaskService;
import com.ggg.taskplatform.task.utils.TpTaskUtils;
import com.magicube.framework.common.constant.UpmsResult;
import com.magicube.framework.common.constant.UpmsResultConstant;
import com.magicube.framework.common.utils.DateFormatUtil;
import com.magicube.framework.common.utils.FatherToChildUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;


import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import java.util.List;

@Controller
@Api(value = "任务反馈")
@RequestMapping("/manage/task/feedback")
public class TaskFeedbackController {
    @Autowired
    TpTaskService tpTaskService;
    @Autowired
    TpTaskFeedbackService tpTaskFeedbackService;

    @ApiOperation("添加任务反馈")
    @RequiresPermissions("tp:task:mytodo")
    @PostMapping("/post")
    @ResponseBody
    @Transactional(rollbackFor = Exception.class)
    public UpmsResult addFeedback(TpTaskFeedback tpTaskFeedback){

        Integer taskId = tpTaskFeedback.getTaskAssociationId();;
        if (ObjectUtils.isEmpty(taskId)){
            return new UpmsResult(UpmsResultConstant.FAILED,"参数错误");
        }

        TpTask tpTask = tpTaskService.selectByPrimaryKey(taskId);
        if (ObjectUtils.isEmpty(tpTask)){
            return new UpmsResult(UpmsResultConstant.FAILED,"任务不存在");
        }

        Subject subject = SecurityUtils.getSubject();
        String curUser = (String) subject.getPrincipal();
        if (!(tpTask.getResponsibleman().equals(curUser)||tpTask.getResponsibleman().equals(curUser+","))&&!TpTaskUtils.getPeopleList(tpTask.getExecutor()).contains(curUser)){
            return new UpmsResult(UpmsResultConstant.FAILED,"不是执行人或责任人");
        }

        tpTaskFeedback.setStatus(TaskFeedbackConstant.FEEDBACK_STATUS_NORMAL);
        tpTaskFeedback.setCtime(System.currentTimeMillis());
        int res = tpTaskFeedbackService.insert(tpTaskFeedback);
        return res>0?new UpmsResult(UpmsResultConstant.SUCCESS,UpmsResultConstant.SUCCESS.getMessage()):new UpmsResult(UpmsResultConstant.FAILED,UpmsResultConstant.FAILED.getMessage());
    }

    @ApiOperation("反馈作废")
    @RequiresPermissions("tp:task:mytodo")
    @GetMapping("/abandon")
    @ResponseBody
    @Transactional(rollbackFor = Exception.class)
    public UpmsResult abandonFeedback(int feedbackId){
        if (feedbackId<=0){
            return new UpmsResult(UpmsResultConstant.FAILED,"参数错误");
        }

        TpTaskFeedback tpTaskFeedback = tpTaskFeedbackService.selectByPrimaryKey(feedbackId);
        if (tpTaskFeedback == null){
            return new UpmsResult(UpmsResultConstant.FAILED,"404-未找到反馈");
        }
        TpTask task = tpTaskService.selectByPrimaryKey(tpTaskFeedback.getTaskAssociationId());
        Subject subject = SecurityUtils.getSubject();
        String curUser = (String) subject.getPrincipal();
        if (!task.getExecutor().contains(curUser)&&!task.getResponsibleman().contains(curUser)&&!task.getInitiator().equals(curUser)){
            return new UpmsResult(UpmsResultConstant.FAILED,"禁止作废");
        }

        tpTaskFeedback.setStatus(TaskFeedbackConstant.FEEDBACK_STATUS_ABANDON);
        int res = tpTaskFeedbackService.updateByPrimaryKey(tpTaskFeedback);
        return res>0?new UpmsResult(UpmsResultConstant.SUCCESS,UpmsResultConstant.SUCCESS.getMessage()):new UpmsResult(UpmsResultConstant.FAILED,UpmsResultConstant.FAILED.getMessage());
    }

    @GetMapping("/get")
    @RequiresPermissions("tp:task:mytodo")
    public String getTaskFeedback(@RequestParam(required = true, defaultValue = "-1", value = "taskId") int taskId, ModelMap modelMap) throws InvocationTargetException {
        TpTaskFeedbackExample tpTaskFeedbackExample = new TpTaskFeedbackExample();
        tpTaskFeedbackExample.or().andTaskAssociationIdEqualTo(taskId);
        List<TpTaskFeedback> feedbacks = tpTaskFeedbackService.selectByExample(tpTaskFeedbackExample);
        List<TpTaskFeedbackChild> tpTaskFeedbackChildren = new ArrayList<>(ObjectUtils.isEmpty(feedbacks)?1:feedbacks.size());
        for(int i=0;i<(ObjectUtils.isEmpty(feedbacks)?0:feedbacks.size());i++){
            TpTaskFeedbackChild tpTaskFeedbackChild = new TpTaskFeedbackChild();
            FatherToChildUtil.fatherToChild(feedbacks.get(i),tpTaskFeedbackChild);
            tpTaskFeedbackChild.setTime(DateFormatUtil.formatTime(tpTaskFeedbackChild.getCtime(),"yyyy-MM-dd"));
            tpTaskFeedbackChildren.add(tpTaskFeedbackChild);
        }
        modelMap.put("feedbacks",tpTaskFeedbackChildren);
        modelMap.put("taskId",taskId);
        return "/manage/task/feedback.jsp";
    }




}
