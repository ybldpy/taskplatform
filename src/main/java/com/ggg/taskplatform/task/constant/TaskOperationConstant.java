package com.ggg.taskplatform.task.constant;

import com.magicube.framework.common.base.BaseConstants;

public class TaskOperationConstant extends BaseConstants {

    public static final byte OTHER = 0;
    public static final byte CREATE_TASK = 1;
    public static final byte MODIFY_TASK = 2;
    public static final byte DELETE_TASK = 3;
    public static final byte ASSIGN_WORKER = 4;
    public static final byte STOP_TASK = 5;
    public static final byte ACTIVE_TASK = 6;
    public static final byte TASK_DONE = 7;
    public static final byte CANCEL_TASK = 8;
    public static final byte CLOSE_TASK = 9;

}
