package com.ggg.taskplatform.task.constant;

import com.magicube.framework.common.base.BaseConstants;


public class TaskFeedbackConstant extends BaseConstants {
    public static final Byte FEEDBACK_STATUS_NORMAL = 1;

    public static final Byte FEEDBACK_STATUS_ABANDON = 0;

    public static final Byte FEEDBACK_SHOW_SHOW=1;
    public static final Byte FEEDBACK_SHOW_NOTSHOW=0;

}
