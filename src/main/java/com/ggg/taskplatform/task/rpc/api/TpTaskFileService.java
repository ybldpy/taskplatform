package com.ggg.taskplatform.task.rpc.api;

import com.magicube.framework.common.base.BaseService;
import com.ggg.taskplatform.task.dao.model.TpTaskFile;
import com.ggg.taskplatform.task.dao.model.TpTaskFileExample;

/**
* TpTaskFileService接口
* Created by justincai on 2023/2/28.
*/
public interface TpTaskFileService extends BaseService<TpTaskFile, TpTaskFileExample> {

}