package com.ggg.taskplatform.task.rpc.service.impl;

import com.magicube.framework.common.annotation.BaseService;
import com.magicube.framework.common.base.BaseServiceImpl;
import com.ggg.taskplatform.task.dao.mapper.TpTaskFileMapper;
import com.ggg.taskplatform.task.dao.model.TpTaskFile;
import com.ggg.taskplatform.task.dao.model.TpTaskFileExample;
import com.ggg.taskplatform.task.rpc.api.TpTaskFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
* TpTaskFileService实现
* Created by justincai on 2023/2/28.
*/
@Service
@Transactional
@BaseService
public class TpTaskFileServiceImpl extends BaseServiceImpl<TpTaskFileMapper, TpTaskFile, TpTaskFileExample> implements TpTaskFileService {

    private static final Log log = LogFactory.getLog(TpTaskFileServiceImpl.class);

    @Autowired
    TpTaskFileMapper tpTaskFileMapper;

}