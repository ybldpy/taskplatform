package com.ggg.taskplatform.task.rpc.api;

import com.magicube.framework.common.base.BaseServiceMock;
import com.ggg.taskplatform.task.dao.mapper.TpTaskFileMapper;
import com.ggg.taskplatform.task.dao.model.TpTaskFile;
import com.ggg.taskplatform.task.dao.model.TpTaskFileExample;

/**
* 降级实现TpTaskFileService接口
* Created by justincai on 2023/2/28.
*/
public class TpTaskFileServiceMock extends BaseServiceMock<TpTaskFileMapper, TpTaskFile, TpTaskFileExample> implements TpTaskFileService {

}
