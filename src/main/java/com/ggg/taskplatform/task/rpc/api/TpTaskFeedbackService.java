package com.ggg.taskplatform.task.rpc.api;

import com.magicube.framework.common.base.BaseService;
import com.ggg.taskplatform.task.dao.model.TpTaskFeedback;
import com.ggg.taskplatform.task.dao.model.TpTaskFeedbackExample;

/**
* TpTaskFeedbackService接口
* Created by justincai on 2023/2/22.
*/
public interface TpTaskFeedbackService extends BaseService<TpTaskFeedback, TpTaskFeedbackExample> {

}