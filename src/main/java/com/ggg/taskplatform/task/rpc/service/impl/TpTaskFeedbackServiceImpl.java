package com.ggg.taskplatform.task.rpc.service.impl;

import com.magicube.framework.common.annotation.BaseService;
import com.magicube.framework.common.base.BaseServiceImpl;
import com.ggg.taskplatform.task.dao.mapper.TpTaskFeedbackMapper;
import com.ggg.taskplatform.task.dao.model.TpTaskFeedback;
import com.ggg.taskplatform.task.dao.model.TpTaskFeedbackExample;
import com.ggg.taskplatform.task.rpc.api.TpTaskFeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
* TpTaskFeedbackService实现
* Created by justincai on 2023/2/22.
*/
@Service
@Transactional
@BaseService
public class TpTaskFeedbackServiceImpl extends BaseServiceImpl<TpTaskFeedbackMapper, TpTaskFeedback, TpTaskFeedbackExample> implements TpTaskFeedbackService {

    private static final Log log = LogFactory.getLog(TpTaskFeedbackServiceImpl.class);

    @Autowired
    TpTaskFeedbackMapper tpTaskFeedbackMapper;

}