package com.ggg.taskplatform.task.rpc.api;

import com.magicube.framework.common.base.BaseServiceMock;
import com.ggg.taskplatform.task.dao.mapper.TpTaskFeedbackMapper;
import com.ggg.taskplatform.task.dao.model.TpTaskFeedback;
import com.ggg.taskplatform.task.dao.model.TpTaskFeedbackExample;

/**
* 降级实现TpTaskFeedbackService接口
* Created by justincai on 2023/2/22.
*/
public class TpTaskFeedbackServiceMock extends BaseServiceMock<TpTaskFeedbackMapper, TpTaskFeedback, TpTaskFeedbackExample> implements TpTaskFeedbackService {

}
