package com.ggg.taskplatform.task.dao.mapper;

import com.ggg.taskplatform.task.dao.model.TpTaskFile;
import com.ggg.taskplatform.task.dao.model.TpTaskFileExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TpTaskFileMapper {
    long countByExample(TpTaskFileExample example);

    int deleteByExample(TpTaskFileExample example);

    int deleteByPrimaryKey(Integer fd);

    int insert(TpTaskFile row);

    int insertSelective(TpTaskFile row);

    List<TpTaskFile> selectByExample(TpTaskFileExample example);

    TpTaskFile selectByPrimaryKey(Integer fd);

    int updateByExampleSelective(@Param("row") TpTaskFile row, @Param("example") TpTaskFileExample example);

    int updateByExample(@Param("row") TpTaskFile row, @Param("example") TpTaskFileExample example);

    int updateByPrimaryKeySelective(TpTaskFile row);

    int updateByPrimaryKey(TpTaskFile row);
}