package com.ggg.taskplatform.task.dao.model;

public class ModifyOperation {
    private Integer taskId;
    private String title;

    private String remark;

    public ModifyOperation() {
    }

    public String getRemark() {
        return remark;
    }

    public ModifyOperation(Integer taskId, String title, String remark, String description, Byte taskType, Byte priority, Byte taskStatus, String taskAssociation, String systemAssociation, String serverAssociation, Byte reportingCycle, String showStarttime, String showEndtime) {
        this.taskId = taskId;
        this.title = title;
        this.remark = remark;
        this.description = description;
        this.taskType = taskType;
        this.priority = priority;
        this.taskStatus = taskStatus;
        this.taskAssociation = taskAssociation;
        this.systemAssociation = systemAssociation;
        this.serverAssociation = serverAssociation;
        this.reportingCycle = reportingCycle;
        this.showStarttime = showStarttime;
        this.showEndtime = showEndtime;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * 任务描述
     *
     * @mbg.generated
     */
    private String description;
    private Byte taskType;
    private Byte priority;
    private Byte taskStatus;
    private String taskAssociation;

    /**
     * 系统关联
     *
     * @mbg.generated
     */
    private String systemAssociation;

    /**
     * 服务器关联
     *
     * @mbg.generated
     */
    private String serverAssociation;

    /**
     * 汇报周期
     0 -- 每天
     1 -- 每3天
     2 -- 每周
     3 -- 每月
     *
     * @mbg.generated
     */
    private Byte reportingCycle;

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Byte getTaskType() {
        return taskType;
    }

    public void setTaskType(Byte taskType) {
        this.taskType = taskType;
    }

    public Byte getPriority() {
        return priority;
    }

    public void setPriority(Byte priority) {
        this.priority = priority;
    }

    public Byte getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(Byte taskStatus) {
        this.taskStatus = taskStatus;
    }

    public String getTaskAssociation() {
        return taskAssociation;
    }

    public void setTaskAssociation(String taskAssociation) {
        this.taskAssociation = taskAssociation;
    }

    public String getSystemAssociation() {
        return systemAssociation;
    }

    public void setSystemAssociation(String systemAssociation) {
        this.systemAssociation = systemAssociation;
    }

    public String getServerAssociation() {
        return serverAssociation;
    }

    public void setServerAssociation(String serverAssociation) {
        this.serverAssociation = serverAssociation;
    }

    public Byte getReportingCycle() {
        return reportingCycle;
    }

    public void setReportingCycle(Byte reportingCycle) {
        this.reportingCycle = reportingCycle;
    }

    public String getShowStarttime() {
        return showStarttime;
    }

    public void setShowStarttime(String showStarttime) {
        this.showStarttime = showStarttime;
    }

    public String getShowEndtime() {
        return showEndtime;
    }

    public void setShowEndtime(String showEndtime) {
        this.showEndtime = showEndtime;
    }

    private String showStarttime;

    /**
     * 截止时间
     */
    private String showEndtime;
}
