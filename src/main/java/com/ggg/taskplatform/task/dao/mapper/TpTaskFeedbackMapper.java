package com.ggg.taskplatform.task.dao.mapper;

import com.ggg.taskplatform.task.dao.model.TpTaskFeedback;
import com.ggg.taskplatform.task.dao.model.TpTaskFeedbackExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TpTaskFeedbackMapper {
    long countByExample(TpTaskFeedbackExample example);

    int deleteByExample(TpTaskFeedbackExample example);

    int deleteByPrimaryKey(Integer feedbackId);

    int insert(TpTaskFeedback row);

    int insertSelective(TpTaskFeedback row);

    List<TpTaskFeedback> selectByExample(TpTaskFeedbackExample example);

    TpTaskFeedback selectByPrimaryKey(Integer feedbackId);

    int updateByExampleSelective(@Param("row") TpTaskFeedback row, @Param("example") TpTaskFeedbackExample example);

    int updateByExample(@Param("row") TpTaskFeedback row, @Param("example") TpTaskFeedbackExample example);

    int updateByPrimaryKeySelective(TpTaskFeedback row);

    int updateByPrimaryKey(TpTaskFeedback row);
}