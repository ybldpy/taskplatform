package com.ggg.taskplatform.task.dao.model;

public class AssignOperation {


    private String operator;
    private String responsibleMan;
    private String executor;

    private String remark;

    private Long ctime;

    private Integer taskId;


    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getResponsibleMan() {
        return responsibleMan;
    }

    public void setResponsibleMan(String responsibleMan) {
        this.responsibleMan = responsibleMan;
    }

    public String getExecutor() {
        return executor;
    }

    public void setExecutor(String executor) {
        this.executor = executor;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public AssignOperation() {
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public String getShowResponsibleMan() {
        return showResponsibleMan;
    }

    public void setShowResponsibleMan(String showResponsibleMan) {
        this.showResponsibleMan = showResponsibleMan;
    }

    public String getShowExecutor() {
        return showExecutor;
    }

    public void setShowExecutor(String showExecutor) {
        this.showExecutor = showExecutor;
    }

    private String showResponsibleMan;
    private String showExecutor;

    @Override
    public String toString() {
        return "AssignOperation{" +
                "operator='" + operator + '\'' +
                ", responsibleMan='" + responsibleMan + '\'' +
                ", executor='" + executor + '\'' +
                ", remark='" + remark + '\'' +
                ", ctime=" + ctime +
                ", taskId=" + taskId +
                ", showResponsibleMan='" + showResponsibleMan + '\'' +
                ", showExecutor='" + showExecutor + '\'' +
                '}';
    }

    public AssignOperation(String responsibleMan, String executor, String remark, Integer taskId, String showResponsibleMan, String showExecutor){
        this.remark = remark;
        this.responsibleMan = responsibleMan;
        this.executor = executor;
        this.taskId = taskId;
    }


}
