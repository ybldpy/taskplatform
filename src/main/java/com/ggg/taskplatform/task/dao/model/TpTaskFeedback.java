package com.ggg.taskplatform.task.dao.model;

public class TpTaskFeedback {
    private Integer feedbackId;

    private Integer taskAssociationId;

    private String feedback;

    private Long ctime;

    private Byte status;

    private Byte showUp;

    public Integer getFeedbackId() {
        return feedbackId;
    }

    public void setFeedbackId(Integer feedbackId) {
        this.feedbackId = feedbackId;
    }

    public Integer getTaskAssociationId() {
        return taskAssociationId;
    }

    public void setTaskAssociationId(Integer taskAssociationId) {
        this.taskAssociationId = taskAssociationId;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Byte getShowUp() {
        return showUp;
    }

    public void setShowUp(Byte showUp) {
        this.showUp = showUp;
    }
}