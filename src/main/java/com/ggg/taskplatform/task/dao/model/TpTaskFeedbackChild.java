package com.ggg.taskplatform.task.dao.model;

public class TpTaskFeedbackChild extends TpTaskFeedback{

    private String time;
    private Byte expires;

    public Byte getExpires() {
        return expires;
    }

    public void setExpires(Byte expires) {
        this.expires = expires;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public void setShowUp(Byte showUp) {
        super.setShowUp(showUp);
    }

    @Override
    public void setStatus(Byte status) {
        super.setStatus(status);
    }

    @Override
    public void setFeedback(String feedback) {
        super.setFeedback(feedback);
    }

    @Override
    public void setFeedbackId(Integer feedbackId) {
        super.setFeedbackId(feedbackId);
    }

    @Override
    public void setCtime(Long ctime) {
        super.setCtime(ctime);
    }

    @Override
    public void setTaskAssociationId(Integer taskAssociationId) {
        super.setTaskAssociationId(taskAssociationId);
    }

}
