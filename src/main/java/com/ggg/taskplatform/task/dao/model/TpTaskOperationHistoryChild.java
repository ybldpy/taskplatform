package com.ggg.taskplatform.task.dao.model;

import java.util.Objects;

public class TpTaskOperationHistoryChild extends TpTaskOperHistory{
    private String operatorRealName;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        TpTaskOperationHistoryChild that = (TpTaskOperationHistoryChild) o;
        return Objects.equals(operatorRealName, that.operatorRealName) && Objects.equals(actionName, that.actionName) && Objects.equals(time, that.time);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), operatorRealName, actionName, time);
    }

    public TpTaskOperationHistoryChild() {
        super();
    }



    private String actionName;
    private String time;

    public String getOperatorRealName() {
        return operatorRealName;
    }

    public String getActionName() {
        return actionName;
    }

    @Override
    public Integer getTaskId() {
        return super.getTaskId();
    }

    @Override
    public String getOperator() {
        return super.getOperator();
    }

    public String getTime() {
        return time;
    }

    @Override
    public Byte getActionType() {
        return super.getActionType();
    }

    @Override
    public Long getCtime() {
        return super.getCtime();
    }

    @Override
    public Integer getId() {
        return super.getId();
    }

    @Override
    public String getRemark() {
        return super.getRemark();
    }

    public void setOperatorRealName(String operatorRealName) {
        this.operatorRealName = operatorRealName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    @Override
    public void setOperator(String operator) {
        super.setOperator(operator);
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public void setActionType(Byte actionType) {
        super.setActionType(actionType);
    }

    @Override
    public void setId(Integer id) {
        super.setId(id);
    }

    @Override
    public void setTaskId(Integer taskId) {
        super.setTaskId(taskId);
    }

    @Override
    public void setCtime(Long ctime) {
        super.setCtime(ctime);
    }

    @Override
    public void setRemark(String remark) {
        super.setRemark(remark);
    }

}
