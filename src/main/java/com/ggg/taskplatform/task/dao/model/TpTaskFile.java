package com.ggg.taskplatform.task.dao.model;

public class TpTaskFile {
    private Integer fd;

    private Integer associationTaskId;

    private String localPath;

    private String url;

    private String fileName;

    private Integer uploadStatus;

    public Integer getFd() {
        return fd;
    }

    public void setFd(Integer fd) {
        this.fd = fd;
    }

    public Integer getAssociationTaskId() {
        return associationTaskId;
    }

    public void setAssociationTaskId(Integer associationTaskId) {
        this.associationTaskId = associationTaskId;
    }

    public String getLocalPath() {
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Integer getUploadStatus() {
        return uploadStatus;
    }

    public void setUploadStatus(Integer uploadStatus) {
        this.uploadStatus = uploadStatus;
    }
}