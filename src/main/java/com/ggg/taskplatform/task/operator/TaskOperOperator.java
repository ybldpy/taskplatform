package com.ggg.taskplatform.task.operator;

import com.ggg.taskplatform.task.dao.model.TpTaskOperHistory;
import com.ggg.taskplatform.task.dao.model.TpTaskOperationHistoryChild;
import com.magicube.framework.common.utils.DateFormatUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class TaskOperOperator {
    @Autowired
    UserOperator userOperator;

    public UserOperator getUserOperator() {
        return userOperator;
    }

    public TaskOperOperator(){
        actionCodeAndName = new HashMap<>();
        actionCodeAndName.put((byte) 0,"其他");
        actionCodeAndName.put((byte) 1,"创建任务");
        actionCodeAndName.put((byte) 2,"修改任务");
        actionCodeAndName.put((byte) 3,"删除任务");
        actionCodeAndName.put((byte) 4,"指派人员");
        actionCodeAndName.put((byte) 5,"暂停任务");
        actionCodeAndName.put((byte) 6,"激活任务");
        actionCodeAndName.put((byte) 7,"完成任务");
        actionCodeAndName.put((byte) 8,"作废任务");
        actionCodeAndName.put((byte) 9,"关闭任务");
    }

    public void setUserOperator(UserOperator userOperator) {
        this.userOperator = userOperator;
    }

    private Map<Byte,String> actionCodeAndName;



    public void covertChildField(TpTaskOperationHistoryChild child){

        if (!StringUtils.isBlank(child.getOperator())){
            child.setOperatorRealName(userOperator.getRealnameByUsername(child.getOperator()));
        }
        child.setTime(DateFormatUtil.formatTime(child.getCtime(),"yyyy-MM-dd"));
        child.setActionName(actionCodeAndName.get(child.getActionType()));
    }
}
