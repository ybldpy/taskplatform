package com.ggg.taskplatform.task.utils;

import com.ggg.taskplatform.task.dao.model.TpTask;
import com.ggg.taskplatform.task.dao.model.TpTaskOperHistory;


public class TaskOperationUtils {



    public static TpTaskOperHistory createOperationHistory(TpTask tpTask,long ctime,String operator,byte actionType,String remark){
        TpTaskOperHistory tpTaskOperHistory = new TpTaskOperHistory();
        tpTaskOperHistory.setTaskId(tpTask.getTaskId());
        tpTaskOperHistory.setActionType(actionType);
        tpTaskOperHistory.setRemark(remark);
        tpTaskOperHistory.setCtime(System.currentTimeMillis());
        tpTaskOperHistory.setOperator(operator);
        return tpTaskOperHistory;
    }

}
