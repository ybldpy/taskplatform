package com.ggg.taskplatform.task.utils;

import java.util.concurrent.atomic.AtomicBoolean;

public class MySimpleLocker{
    private AtomicBoolean atomicBoolean;

    public void lock(){
        while (!atomicBoolean.compareAndSet(true,false)){

        }
    }
    public void unlock(){
        atomicBoolean.compareAndSet(false,true);
    }

    public MySimpleLocker(){
        atomicBoolean = new AtomicBoolean(true);
    }
}
