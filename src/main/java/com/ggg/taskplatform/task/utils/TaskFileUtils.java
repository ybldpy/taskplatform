package com.ggg.taskplatform.task.utils;

import com.ggg.taskplatform.task.dao.model.TpTaskFile;
import org.apache.commons.io.FileUtils;
import org.springframework.util.DigestUtils;
import org.springframework.util.ObjectUtils;

import java.io.*;
import java.util.List;

public class TaskFileUtils {

    public static File createTaskFile(String parentPath, String path) throws IOException {

        File file = new File(parentPath+path);
        file.createNewFile();
        return file;

    }

    public static void readFileToStream(InputStream inputStream,OutputStream outputStream,int chunkSize) throws IOException {
        byte[] data = new byte[chunkSize];
        int read = -1;
        while ((read=inputStream.read(data))!=-1){
            outputStream.write(data,0,read);
        }
    }

    public static File createTempFile(String prefix,String suffix,File dir,long size) throws IOException {
        File exstingTempFile = new File(dir.getAbsolutePath()+"\\"+prefix+suffix);
        if (exstingTempFile.exists()){
            exstingTempFile.delete();
        }
        suffix = null==suffix?".tmp":"".equals(suffix)?".tmp":suffix;
        File file = new File(dir.getAbsolutePath()+"\\"+prefix+suffix);
        file.createNewFile();
        RandomAccessFile randomAccessFile = new RandomAccessFile(file,"rw");
        randomAccessFile.seek(size-1L);
        randomAccessFile.write(new byte[]{0});
        randomAccessFile.close();
        return file;
    }

    public static void hideFileLocalPath(TpTaskFile tpTaskFile){
        if (ObjectUtils.isEmpty(tpTaskFile)){return;}
        tpTaskFile.setLocalPath("");
    }
    public static void hideFileLocalPath(List<TpTaskFile> tpTaskFiles){
        if (ObjectUtils.isEmpty(tpTaskFiles)){return;}
        for(TpTaskFile tpTaskFile:tpTaskFiles){
            hideFileLocalPath(tpTaskFile);
        }
    }

    public static String getMd5(byte[] bytes){
        return DigestUtils.md5DigestAsHex(bytes);
    }

    public static void writeFile(File file,int index,long chunkSize,byte[] bytes) throws IOException {

        RandomAccessFile randomAccessFile = new RandomAccessFile(file,"rw");
        randomAccessFile.seek(index*chunkSize);
        randomAccessFile.write(bytes);
        randomAccessFile.close();

    }


    public static void writeTempFileToFileDir(File tempFile,String path) throws IOException {
        File file = new File(path);
        file.createNewFile();
        FileUtils.copyFile(tempFile,file,false);
        tempFile.delete();


    }
}
