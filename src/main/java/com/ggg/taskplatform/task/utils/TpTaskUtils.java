package com.ggg.taskplatform.task.utils;
import com.baidu.unbiz.fluentvalidator.ValidationError;
import com.ggg.taskplatform.task.dao.model.TpTaskChild;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

public class TpTaskUtils {
    public static Set<Integer> covertIdStringToSet(String idStr){
        Set<Integer> integers = new HashSet<>();
        if (StringUtils.isBlank(idStr)){
            return integers;
        }
        String[] ids = idStr.split(",");
        for(int i=0;i< ids.length;i++){
            integers.add(Integer.parseInt(ids[i]));
        }
        return integers;
    }
    public static String getValidationErrorMessage(List<ValidationError> errors){
        StringBuilder sb = new StringBuilder();
        for(ValidationError error:errors){
            sb.append(error.getErrorMsg());
            sb.append(",");
        }
        return sb.toString().substring(0,sb.length()-1);
    }
    public static List<String> getPeopleList(String people){
        List<String> peopleList = new ArrayList<>();
        if (StringUtils.isBlank(people)){
            return peopleList;
        }
        String[] peoples = people.split(",");
        for(String str:peoples){
            peopleList.add(str);
        }
        return peopleList;
    }
    public static Map<String,Object> createAssociationTasksListResultMap(int rows,List<TpTaskChild> list){
        Map<String,Object> map = new HashMap<>();
        map.put("total",rows);
        map.put("rows",list);
        return map;
    }
}
