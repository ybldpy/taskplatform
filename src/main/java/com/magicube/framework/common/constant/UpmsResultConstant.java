package com.magicube.framework.common.constant;

/**
 * upms系统接口结果常量枚举类
 *
 * @author justincai
 */
public enum UpmsResultConstant {

    FAILED(0, "failed"),
    SUCCESS(1, "success"),
    INVALID_LENGTH(10001, "Invalid length"),
    EMPTY_USERNAME(10101, "Username cannot be empty"),
    EMPTY_PASSWORD(10102, "Password cannot be empty"),
    INVALID_USERNAME(10103, "Account does not exist"),
    INVALID_PASSWORD(10104, "Password error"),
    INVALID_ACCOUNT(10105, "Invalid account"),
    EMPTY_RESPONSIBLEMAN(10103,"responsible man cannot be empty"),
    NOT_INITIATOR_OR_RESPONSIBLEMAN(10104,"you are not initiator or responsible man");

    public int code;

    public String message;

    UpmsResultConstant(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
